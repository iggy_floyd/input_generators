#!/usr/bin/env bash


#@author @2014 Igor Marfin [Unister Gmbh] <igor.marfin@unister.de>
#@description  makes a documentation from  python docstrings of sources in the confluence wiki and RsT formats.

export PYTHONPATH=`pwd`/PyLit
_pylit_bin='PyLit/pylit.py'

# main function
main() {

[[ $# -lt 1 ]] &&
(echo "Error: <WRONG_NUMBER_ARGUMENTS>"; echo; echo "Usage: $0 \`ls ../lib/*.py\`";echo;echo )     &&
 return 1;

for arg in $@
do
    echo $arg;
    [[ $(basename $arg) =~ ^_ ]] && echo "This is an internal file of the package and it can not be processed for documentating... skippig..." && continue;
   
    local _tmp_dir=`basename \`dirname ${arg}\``;
    mkdir $_tmp_dir 2>/dev/null;
    filename=$(basename "$arg")
    extension="${filename##*.}"
    if [ "$extension" = "py" ]
    then
        $_pylit_bin -c $arg;
        mv ${arg}.txt `basename ${arg}.rst`;
        mv `basename ${arg}.rst`  $_tmp_dir;
    elif [ "$extension" = "sh" ]  
    then
        ./shell2rst.py $arg $_tmp_dir/`basename ${arg}.rst`
    fi
    
done
#return $?;
return 0;
}


main $@
#[[ $? -eq 0 ]] && [[ `which restview` ]] &&  restview -l *:8095 */*.txt
