#!/usr/bin/env bash


#@author @2016 Igor Marfin [Inducon] <igor.marfin@inducon.de>
#@description:  creates and serve the documentation from RsT files.


# to serve only Readme file
#rst-tool/create_docs.sh /package/README.rst `basename $(pwd)` Documentation ; mv README.html docs;
#pushd docs/;    python -m SimpleHTTPServer 8010; popd;

# to serve Readme and source documentation
cd source_doc/; ./create_documentation.sh `ls /package/*.py` `ls /package/*.sh` 
rm -rf ../docs/package
mv package ../docs
cd ../docs
cp /package/README.rst .
#restview -l *:8095 */*.rst
watchmedo shell-command  --patterns="*.py;*.rst"  --recursive --command='/app/render_doc.sh' /package/ &
restview -l *:8095 *.rst
#restview -l *:8095 .
