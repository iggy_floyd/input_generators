================================================================================================================================
A Car Simulation Tool 
================================================================================================================================






:Author: Dr. Igor Marfin
:Contact: igor.marfin@robotron.de
:Organization: Robotron Datenbank-Software GmbH
:Date: Dec 30, 2016, 9:28:51 AM
:Status: draft
:Version: 1
:Copyright: This document has been placed in the public domain. You
            may do with it as you wish. You may copy, modify,
            redistribute, reattribute, sell, buy, rent, lease,
            destroy, or improve it, quote it at length, excerpt,
            incorporate, collate, fold, staple, or mutilate it, or do
            anything else to it that your or anyone else's heart
            desires.


|
|


.. admonition:: Dedication

    For users & co-developers.



|
|


.. admonition:: Abstract

    This  project is a tool which performs the simulation of the data transfer between cars moving in metropolices and a database



|
|    

.. meta::
   :keywords: Robotron, Car, BMW, Metropolis
   :description lang=en: Simulation tool for the data transfer between moving cars in metropolices and the database



|
|




.. contents:: Table of Contents





----------------------------------------------------------------
Introduction to the topic and motivation
----------------------------------------------------------------


This is a nice tool to be used to simulate the data transfer from cars to the database.







----------------------------------------------------------------
Installation of the package
----------------------------------------------------------------


.. Important::
  
    
    The project relies on the access to the private git repository where all needed files are stored.




Getting Started
======================================

The project requires that ``docker`` and ``docker-compose`` [#DOCKER]_ tools are presented in the system. I refer to the following installation instructions for ubuntu [#DOCKER]_.
 


Clone the project
--------------------------------------

Before, to proceed further, please clone files from the private repository [#REPO]_. To get access to it, please 
ask permissions from Igor Marfin [#IGOR]_. 


Then, do

.. code-block:: 

   git clone git@bitbucket.org:iggy_floyd/input_generators.git
   cd input_generators

When you are ready with cloning, simply, build and start the ``docker-compose`` cluster

.. code-block:: 

    docker-compose build
    docker-compose up -d


That's it.



----------------------------------------------
Quick Tutorial on Use Cases
----------------------------------------------

We focus on a few topic useful to start the simulations. They are 

* `Configuration`_
* `Starting the simulation`_
* `Monitoring the simulation`_
* `Terminating the simulation`_


Configuration
======================


The folder ``package/`` of the ``input_gerenators`` contains among python source files also the ``config.ini`` file  having the following format:


.. code-block::  ini

   [Simulation]
   RealTimeSimulation=true
   Duration=1
   SizePoolThreads=30
   #VirtualTimeFactor=0.001
   #VirtualTimeFactor=0.0001
   VirtualTimeFactor=0.01

   [Metropolis]
   # Large: 30x30 km^2
   # Medium: 20x20 km^2
   # Small: 10x10 km^2
   Size=Large
   #Cars=10000000
   Cars=2000
   # grid precision: 5 -- 4x4 km^2 of the cell size
   # 6 -- 0.6x0.6 km^2
   Precision=5

   [Model]
   Multicolumn=true



Here the parameters of the configuration file define the properties of the simulation process:

* ``RealTimeSimulation=true|false``  defines the mode of the simulation, either it is an initial data load or a real-time-simulation;
* ``Duration=1``  corresponds to a number of simulated days;  
* ``SizePoolThreads=30`` determines the number of workers in the thread pool;
* ``VirtualTimeFactor=0.01`` determines the speed of the real-time simulation;

  * the smaller ``VirtualTimeFactor`` parameter is, the faster real-time simulation will be done;

* ``Size=Large`` provides the size of the metropolis;

  * ``Large`` corresponds to the 30x30 km^2 area;
  * ``Medium`` corresponds to the 20x20 km^2 area;
  * ``Small`` corresponds to the 10x10 km^2 area;

* ``Cars=2000`` determines the number of cars in the metropolis;
* ``Precision=5`` provides the size of cells in the metropolis grid;

  * ``5`` corresponds to the 4x4 km^2 grid cell;
  * ``6`` corresponds to the 0.6x0.6 km^2 grid cell;

* ``Multicolumn=true|false`` defines the format of the table in the database.


Starting the simulation 
================================


To start simulation, one can simply send a ``GET`` request in a browser to the following url:


.. code-block::

  # in the firefox, type the url and press ENTER
  firefox http://13.81.206.155:5000/simulate
  # or 
  curl http://13.81.206.155:5000/simulate




Monitoring the simulation 
================================


The online monitor, providing statuses of simulation tasks, is available under the link:


.. code-block::

  # in the firefox, type the url and press ENTER
  firefox http://13.81.206.155:5555
  # or
  curl http://13.81.206.155:5555





Terminating the simulation 
================================

One can terminate simulation by requesting the following url:

.. code-block::

  # in the firefox, type the url and press ENTER
  firefox http://13.81.206.155:5000/terminate
  # or
  curl http://13.81.206.155:5000/terminate


----------------------------------------------------------------
Practical Hints on the simulation parameters
----------------------------------------------------------------

There are a few tricks about parameters specified in the ``config.ini`` file, which can change 
workflow of the simulation process.

``SizePoolThreads`` parameter
================================

This parameter plays the significant role in case of so called *initial-load-database* simulation, i.e
when the parameter ``RealTimeSimulation`` is ``false``. Larger the parameter is, more data can be uploaded to the database per second.

The ``SizePoolThreads`` defines the size of the *workers* pool. These *workers* concurrently commit the simulated data to the database.
If the number of workers is larger than the size of the connection pool provided by the database, there is possibility that some of transactions will be 
failed because of the absence of available open connections.

For example, Postgresql provides a pool of 50 connections by default. So it is recommended to set the ``SizePoolThreads`` as 50. 
If you don't carry about transaction fails but you are interested to fill database as fast as possible, 
then you might want to siginificantly increase the number of workers, i.e. ``SizePoolThreads`` can be 500.

.. Important::

   You can change this parameter to get effect without restarting the ``docker-compose`` services.

      
``Multicolumn`` parameter
================================

This parameter defines the database schema. If it is ``true`` then all randomly generated attributes of the data are placed in specific column of the database table ``cars``.
For example, the value of the ``bcBeTripDistanceKm`` *(Trip Distanz)* attribute defined for the *Fahrtenhistorie* Event will be stored in the column named ``bcBeTripDistanceKm``.

If the ``Multicolumn`` parameter is ``false`` then all attributes of the Event are stored in the **HSTORE** format in the column ``eventdata`` of the ``cars`` table.


.. Important::

   If you decide to change the value of the parameter to switch the database schema, you need to restart the ``worker`` service:

   .. code-block::
    
      cd input_generators

      # change Multicolumn parameter
      nano package/config.ini

      # restart worker service
      docker-compose restart worker   


----------------------------------------------------------------
Logging
----------------------------------------------------------------

The system also produces log files where information about critical fails or important simulation steps are stored.
The log files are stored in the folder ``log/``.

.. code-block::

   cd input_generators
   ls log/

   ......

    .dir  log_05_01_2017

For example, you can observe problems with transactions if you have set a quite large value of the ``SizePoolThreads`` parameter:

.. code-block::

   cd input_generators
   cat log/log_05_01_2017

   ........

   [Thu Jan  5 12:12:34 2017]  [*file*: /flask-celery/tasks.py *function*: simulate  *line*: 179]  [INFO]  [  SIMULATION 0: SIMULATION_STARTED  ]
   
   ........

   [Thu Jan  5 13:39:22 2017]  [*file*: /flask-celery/package/car.py *function*: simulate  *line*: 387]  [ERROR]  [  Event 86 EVENT_RECORD_PROBLEM  ]
   [Thu Jan  5 13:39:22 2017]  [*file*: /flask-celery/package/car.py *function*: simulate  *line*: 387]  [ERROR]  [  Event 88 EVENT_RECORD_PROBLEM  ]
   [Thu Jan  5 13:39:22 2017]  [*file*: /flask-celery/package/car.py *function*: simulate  *line*: 387]  [ERROR]  [  Event 92 EVENT_RECORD_PROBLEM  ]
   [Thu Jan  5 13:39:22 2017]  [*file*: /flask-celery/package/car.py *function*: simulate  *line*: 387]  [ERROR]  [  Event 101 EVENT_RECORD_PROBLEM  ]
   [Thu Jan  5 13:39:22 2017]  [*file*: /flask-celery/package/car.py *function*: simulate  *line*: 387]  [ERROR]  [  Event 111 EVENT_RECORD_PROBLEM  ]

   ........
     
   [Thu Jan  5 13:40:51 2017]  [*file*: /flask-celery/tasks.py *function*: revoked_handler  *line*: 52]  [ERROR]  [  SIMULATION 0: SIMULATION_INTERRUPTED  ]



----------------------------------------------------------------
Documentation
----------------------------------------------------------------

The main parts of documentation are placed  in the ``package``/``README.rst``.

The python codes in ``package/`` folder are documented as well.
The docstring are used for this purpose.

The ``Pylit`` frameworks and  two provided scripts: ``source_doc/create_documentation.sh`` and
``source_doc/shell2rst.py`` are used to extract the docstrings and create 
the documentation of the source code as ``*.rst`` files.


Running the command,

.. code-block:: 
    
    docker-compose build docserver
    docker-compose up -d docserver
     
     
    
    
one serves the ``documentation`` server which is available at the ``*:8095`` port.


.. code-block::

  # in the firefox, type the url and press ENTER
  firefox http://13.81.206.155:8095
  




Documenation of the source code
===================================

Here you can find the source files and documentation on it.	

* `utils.py <package/utils.py.rst>`_
* `cars.py <package/car.py.rst>`_
* `simulator.py <package/simulator.py.rst>`_ 
* `RedisListener.py <package/RedisListener.py.rst>`_ 
* `multithreads.py <package/multithreads.py.rst>`_ 
* `motion.py <package/motion.py.rst>`_ 
* `model.py <package/model.py.rst>`_ 
* `geohash.py <package/geohash.py.rst>`_ 
* `events.py <package/events.py.rst>`_ 
* `datatypes.py <package/datatypes.py.rst>`_ 
* `generators.py <package/generators.py.rst>`_ 
* `configurator.py <package/configurator.py.rst>`_ 
* `logger.py <package/logger.py.rst>`_ 

.. it can be an embedded  part of the documentation 
.. #include:: package/utils.py.rst


----------------------------------------------------------------
References
----------------------------------------------------------------


.. [#DOCKER]  https://docs.docker.com/engine/installation/

              https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-16-04
 

.. [#REPO] https://bitbucket.org/iggy_floyd/input_generators
.. [#IGOR] Dr. Igor Marfin <igor.marfin@robotron.de>

.. target-notes::
    



