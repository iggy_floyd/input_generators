#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Event Types
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   events.py
#

'''
	Event Types

'''

__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::


import numpy as np


# ``Event``
# ------------
# The ``Event`` class is a basic class to randomly generate events according the Poisson distribution
# 


class Event(object):
    def __init__(self,name,generator,mu):
        self.name=name
        self.generator=generator
        self.mu = mu
        
    def generate(self):
        return self.generator(None)[0]

# ``event_list``
# --------------
# The ``event_list`` list determines all possible events and their rates during a day. 
# 

event_list = [
    # name of the event, mu of the poisson
    ("Fahrtenhistorie",3.5),
    ("Ladehistorie",1.4),
    ("Leistungshistorie",1.4),
    ("Steckhistorie",1.4),
    ("Tankhistorie",0.12),    
]
    
# ``EventProducer``
# -----------------------
# The ``EventProducer``  generates ``Event`` for the defined types
# 

def EventProducer(event_list):
        for event in event_list:
            yield Event(event[0],lambda x: np.random.poisson(event[1], 1),event[1])
            
            
class Event2(object):
    def __init__(self,name,mu):
        self.name=name
        self.mu = mu
        
    def generate(self):
        return  np.random.poisson(self.mu,1)[0]
    

EventList = [ Event2(evt[0],evt[1]) for evt in event_list]
