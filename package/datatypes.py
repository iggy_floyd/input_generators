#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Data Types
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   datatypes.py
#

'''
	Event Types

'''

__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::


from .generators import *

# ``DataType``
# --------------------
# The ``DataType`` class is a helper class which translates an arbitrary dictionary to object instance
# http://stackoverflow.com/questions/1639174/creating-class-instance-properties-from-a-dictionary-in-python
# http://stackoverflow.com/questions/22468401/can-you-set-a-dictionary-value-dependant-on-another-dictionary-entry

class DataType(dict):
    def __init__(self,namedata,dicti):
        super(DataType, self).__init__(dicti)
        self.namedata=namedata
        
    def __getattr__(self, name):
        return self[name]
    
    def __getitem__(self, key):
        #if '_' in key:
        #    return None
        value = super(DataType, self).__getitem__(key)            
        return eval(value, self) if isinstance(value, str) else value    

    

    
# ``Callable``
# --------------------
# The ``DataType`` class is a helper class which executes a pythonic expression

class Callable(object):
    def __init__(self,expr):
        self.expr=expr
        
    def __call__(self):
        return self.expr
    
# Daten fuer Fahrtenhistorie
# ---------------------------------
#

FahrtenhistorieData={
    # name of the parameter: generator
    "VIN":VINCodeGenerator,
    "maxBatterySize":maxBatterySizeGenerator,
    "engine_type":EngineTypeGenerator,
    "modelcode":ModelCodeGenerator,
    "bcBeSegmentId":RandomBeSegmentIdGenerator,
    "bcBeTimeStartLocal":DatetimeInSecs,
    "bcBeTripDistanceKm":RandomBeTripDistanceKmGenerator,
    "bcBeSpeedAvgDrive":RandomAvrageSpeedGenerator,
    "bcBeDurationDrive":"lambda : (bcBeTripDistanceKm()[0]/bcBeSpeedAvgDrive()[0]*3600.,4)",    
    # hackish way to use the previous elements of the dictionary
    "_RandomBeTimeEndLocalGenerator":RandomBeTimeEndLocalGenerator,
        "bcBeTimeEndLocal":"lambda: _RandomBeTimeEndLocalGenerator(bcBeTimeStartLocal()[0])",
    "_RandomBeTimeStartUTCGenerator":RandomBeTimeStartUTCGenerator,
    "_callable":Callable,
    "_bcBeTimeStartUtc":"(_RandomBeTimeStartUTCGenerator(bcBeTimeEndLocal()[0])[0]-bcBeDurationDrive()[0],8)",
    "bcBeTimeStartUtc":"_callable(_bcBeTimeStartUtc)",
    "bcBeTimeEndUtc":DatetimeInSecs,
    "bcBeConsFuel_visible":RandomBeConsFuelVisibleGenerator,
    "bcBeConsElectric_visible":RandomBeConsElectricVisibleGenerator,
    "bcBeConsElectricAll":RandomBeConsElectricAllGenerator,
    "bcBeRecuperationAll":RandomBeRecuperationAllGenerator,
    "bcBeNvConsAll":RandomBeNvConsAllGenerator,
    "_RandomBeTripElectricKmGenerator":RandomBeTripElectricKmGenerator,
    "bcBeTripElectricKm":"lambda : _RandomBeTripElectricKmGenerator(bcBeTripDistanceKm()[0])",
    "bcBeHvSocStart": RandomBeHvSocStartGenerator,
    "bcBeHvSocEnd": RandomBeHvSocEndGenerator,
    "bcBeTankLevelStart": RandomBeTankLevelStartGenerator,
    "_RandomBeTankLevelEndGenerator":RandomBeTankLevelEndGenerator,
    "bcBeTankLevelEnd": "lambda : _RandomBeTankLevelEndGenerator(bcBeTankLevelStart()[0])",
    "bcBeDriverId": RandomBeDriverIdGenerator,
    "GPS_Start_Lat": RandomGPSStartLatGenerator,
    "GPS_Start_Lon": RandomGPSStartLonGenerator,
    "GPS_End_Lat":RandomGPSEndLatGenerator,
    "GPS_End_Lon":RandomGPSEndLonGenerator    
}



# Daten fuer Steckhistorie
# ---------------------------------
#

SteckhistorieData={
    "SEG_X_PLUG_SEGMENT_ID":RandomSEGXPLUGSEGIDGenerator,
    "SEG_X_TIME_UTC0_PLUG":RandomSEGXTIMELocalPlugGenerator,
    "_RandomBeTimeEndLocalGenerator": RandomBeTimeEndLocalGenerator,
    "SEG_X_TIME_UTC0_UNPLUG":"lambda: _RandomBeTimeEndLocalGenerator(SEG_X_TIME_UTC0_PLUG()[0])",
    "SEG_X_TIME_LOCAL_PLUG":"SEG_X_TIME_UTC0_PLUG",
    "SEG_X_POS_LAT":RandomLatSEGXPOSGenerator,
    "SEG_X_POS_LON":RandomLonSEGXPOSGenerator,
    "SEG_X_SOC_CUST_PLUG":RandomSEGXSOCCUSTPLUGGenerator,
    "SEG_X_SOC_CUST_UNPLUG":RandomSEGXSOCCUSTUNPLUGGenerator,
    "SEG_X_ENERGY_CONS_FROM_POWERGRID": RandomSEGXENERGYCONSFROMPOWERGRIDGenerator    
    
}

# Daten fuer Ladehistorie
# ---------------------------------
#

LadehistorieData={
    "SEG_X_CHARGE_SEGMENT_ID":RandomSEGXCHARGESEGIDGenerator,
    "SEG_X_TIME_UTC0_START":RandomSEGXTIMELocalStartGenerator,
    "_RandomBeTimeEndLocalGenerator": RandomBeTimeEndLocalGenerator,
    "SEG_X_TIME_UTC0_END":"lambda: _RandomBeTimeEndLocalGenerator(SEG_X_TIME_UTC0_START()[0])",
    "SEG_X_TIME_LOCAL_START":"SEG_X_TIME_UTC0_START",
    "SEG_X_SOC_CUST_START":RandomSEGXSOCCUSTSTARTGenerator,
    "_RandomSEGXSOCCUSTENDGenerator":RandomSEGXSOCCUSTENDGenerator,
    "SEG_X_SOC_CUST_END": "lambda : _RandomSEGXSOCCUSTENDGenerator(SEG_X_SOC_CUST_START()[0])",
    "SEG_X_ENERGY_CONS_FROM_POWERGRID":RandomSEGXENERGYCONSFROMPOWERGRIDGenerator
}
    

# Daten fuer Leistungshistorie
# ---------------------------------
#

LeistungshistorieData= {
    "CHARGE_SEGMENT_ID_ALL":RandomSEGXCHARGESEGIDGenerator,
    "TIME_UTC0_CHARGE_START":RandomSEGXTIMELocalStartGenerator,
    "_RandomBeTimeEndLocalGenerator": RandomBeTimeEndLocalGenerator,
    "TIME_UTC0_CHARGE_END":"lambda: _RandomBeTimeEndLocalGenerator(TIME_UTC0_CHARGE_START()[0])",
    "SLOT_LENGTH_MINUTES":RandomSLOTLENGTHMINUTESGenerator
}
LeistungshistoriePOWER_AT_PLUG_SLOTData=[("POWER_AT_PLUG_SLOT_%d"%i,RandomPOWERATPLUGSLOTGenerator) for i in range(1,97)]
LeistungshistorieData = dict(LeistungshistorieData.items()+LeistungshistoriePOWER_AT_PLUG_SLOTData)


# Daten fuer Tankhistorie
# ---------------------------------
#

TankhistorieData = {
    "bcBeSegmentId":RandomBeSegmentIdTankhistorieGenerator,
    "bcBeTimeEndLocal":DatetimeInSecs,
    "bcBeTimeEndUtc":DatetimeInSecs,
}

# ``datatypes_list``
# ---------------------------------
# List of all data types
#

datatypes_list = [
    # name of the datatype, datatype
    ("Fahrtenhistorie",FahrtenhistorieData),
    ("Steckhistorie",SteckhistorieData),
    ("Ladehistorie",LadehistorieData),
    ("Leistungshistorie",LeistungshistorieData),
    ("Tankhistorie",TankhistorieData)
]

# ``DataTypeProducer``
# ---------------------------------
# generates the ``DataType`` instances from the ``datatypes_list``
#

def DataTypeProducer(datatypes_list):
        for datatype in datatypes_list:
            yield DataType(datatype[0],datatype[1])
            
