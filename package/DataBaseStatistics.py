#! /usr/bin/env python
# -*- coding: utf-8 -*-

# DataBaseStatistics
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   DataBaseStatistics.py
#

'''
	The DataBaseStatistics class

'''


__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::


import sys
import time
import  datetime
from blinker import Signal
import traceback
import threading
import redis
#import redis_lock
from .logger import mylogging
from .configurator import Configurator
config=Configurator('package/config.ini').get_config()
config_fields=["ThreadPoolSize","numCars","RealTimeSimulation","MetropolisSize"]
mylogging=mylogging.setSimConfig(dict(zip(config_fields, [config[k] for k in config_fields])))
mylogging.print2stdout=False
_mylogging=lambda x: mylogging.setVerbosity('INFO')(x)

# RepeatedTimer
# --------------------
# N/D
# 


class RepeatedTimer:
    
    '''
    the class responsible for repeating some function in time (per 1 s second), for example
    >timer = RepeatedTimer(1, lambda: print_percent(total, BIGFILE_SIZE))
    >timer.start()
    >try:
    >    while 1:
    >        f.write(chunk)
    >        total += chunk_len
    >        if total >= BIGFILE_SIZE:
    >            break
    >finally:
    >    f.close()
    >    timer.stop()
    '''

    def __init__(self, timeout, fun):
        self.timeout = timeout
        self.fun = fun

    def start(self):
        
        def main():
            self.fun()
            self.start()

        self.timer = threading.Timer(1, main)
        self.timer.start()


    def stop(self):
        self.timer.cancel()


# DatabaseManager
# --------------------
# N/D
# 


class DatabaseManager(object):

  successed = Signal()
  failed= Signal()
  redis_host = "redis"
  redis_port = 6379
  myr = redis.StrictRedis(host=redis_host, port=redis_port, db=0)
  ok=0
  bad=0
  ok_id=[]
  bad_id=[]
  prev_ok=0
  prev_bad=0
  prev_ok_time=datetime.datetime.now()
  prev_bad_time=datetime.datetime.now()
  start_time=datetime.datetime.now()
  timer=None
  #timer = RepeatedTimer(1, lambda: DatabaseManager.timerjob())
  

  @staticmethod
  def  timerjob():
    _mylogging("OK_COMMITS_PER_SEC %d"%(len(DatabaseManager.ok_id)-DatabaseManager.prev_ok))
    _mylogging("AVG_OK_COMMITS_PER_SEC %f"%(float(len(DatabaseManager.ok_id)-DatabaseManager.prev_ok)/(datetime.datetime.now()-DatabaseManager.start_time).total_seconds()))
    DatabaseManager.prev_ok=len(DatabaseManager.ok_id) 
    _mylogging("BAD_COMMITS_PER_SEC %d"%(len(DatabaseManager.bad_id)-DatabaseManager.prev_bad))
    _mylogging("AVG_BAD_COMMITS_PER_SEC %f"%(float(len(DatabaseManager.bad_id)-DatabaseManager.prev_bad)/(datetime.datetime.now()-DatabaseManager.start_time).total_seconds()))
    DatabaseManager.prev_bad=len(DatabaseManager.bad_id) 
    _mylogging("TOTAL_OK_COMMITS %d"%DatabaseManager.prev_ok)
    _mylogging("TOTAL_BAD_COMMITS %d"%DatabaseManager.prev_bad)



  @staticmethod
  @successed.connect
  def success(sender_id):
   if DatabaseManager.timer is None:
    _mylogging("started")
    DatabaseManager.timer =  RepeatedTimer(1, DatabaseManager.timerjob)
    DatabaseManager.timer.start()
    DatabaseManager.start_time=datetime.datetime.now()
   #with redis_lock.Lock(DatabaseManager.myr, "_ok_lock_"):
   DatabaseManager.ok+=1
   DatabaseManager.ok_id.append(sender_id)

  @staticmethod
  @failed.connect
  def fail(sender_id):
   if DatabaseManager.timer is None:
    _mylogging("started")
    DatabaseManager.timer =  RepeatedTimer(1, DatabaseManager.timerjob)
    DatabaseManager.timer.start()
    DatabaseManager.start_time=datetime.datetime.now()
   #with redis_lock.Lock(DatabaseManager.myr, "_notok_lock_"):
   DatabaseManager.bad+=1
   DatabaseManager.bad_id.append(sender_id)

  def __repr__(self):
   return str({"OK commits":DatabaseManager.ok,"BAD":DatabaseManager.bad, 
		#"ok_id":DatabaseManager.ok_id,
		#"bad_id":DatabaseManager.bad_id,
	})
  
  def  __del__(self):
    _mylogging( "stoping timer" )
    if (DatabaseManager.timer):
     DatabaseManager.timer.stop()


#manager=DatabaseManager()
#timer = RepeatedTimer(1,  DatabaseManager.timerjob)
#timer.start()

