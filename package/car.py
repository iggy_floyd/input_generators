#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Car
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   car.py
# :Copyright: @ Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de
#


'''
	Car definition
'''



__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::

import numpy as np
from .datatypes import DataTypeProducer
from .events import EventProducer,EventList
from .utils import DatetimeInSecs
from .utils import makeUUID
from .utils import histogram
from .utils import generateEventTimeWithinDay
import datetime
from .geohash import * 
from threading import  current_thread
from .model import *
from .multithreads import *
from .motion import *
from memory_profiler import profile
import traceback
from .utils import VirtualDateTime,getSecondBin
from .logger import mylogging
from .DataBaseStatistics import DatabaseManager,RepeatedTimer



import threading
class StaticEvent(object):
 eventid=-1

lock= threading.Lock()

# Car class
# ------------
# The class ``Car`` defines the car objects.
# 

class Car(object):
    counter = 0 # unique id of the car
    def __init__(self,idnum=makeUUID(),eventList=None,dataList=None,geohash=None,metropolis=None):
        self.day_simulation=0
        self.day_of_week=-1
        self.lat=decode_exactly(geohash)[0]
        self.lon=decode_exactly(geohash)[1]
        self.prevlat=decode_exactly(geohash)[0]
        self.prevlon=decode_exactly(geohash)[1]        
        self.geohash=geohash
        self.metropolis=metropolis
        self.today=datetime.datetime.fromtimestamp(DatetimeInSecs()[0])        
        self.idnum=idnum       
        #self.events=EventList
        self.events=eventList        
        self.data=dataList
        self.counter=Car.counter
        Car.counter+=1
        
    def GetDistanceKmGeoCodeModel(self):
        code=self.geohash
        metropolis = self.metropolis
        distance=RandomDistanceGenerator(code,metropolis)        
        return (float(distance)/1000.,4)

    def GeoCodeModel(self):
        code=self.geohash
        metropolis = self.metropolis
        RandomCellGenerator(code,metropolis) # make a random movement
        self.geohash=RandomGeohashGenerator(code,metropolis)                
        self.prevlat = self.lat
        self.prevlon = self.lon
        
    
    def GetStartLat(self):        
        return (self.prevlat,4)
    
    def GetStartLon(self):        
        return (self.prevlon,4)
    
    def SetLatGeoCodeModel(self):        
        code=self.geohash
        metropolis = self.metropolis        
        newLatLon=RandomLatLonGenerator(code,metropolis)
        self.lat = newLatLon[0]
        return (self.lat,4)

    def SetLonGeoCodeModel(self):
        code=self.geohash
        metropolis = self.metropolis        
        newLatLon=RandomLatLonGenerator(code,metropolis)        
        self.lon = newLatLon[1]
        return (self.lon,4)

    def ReplaceGenerators(self,data):
        if 'GPS_Start_Lat' in data.keys():
            data['GPS_Start_Lat'] = self.GetStartLat
        if 'GPS_Start_Lon' in data.keys():            
            data['GPS_Start_Lon'] = self.GetStartLon        
        if 'GPS_End_Lat' in data.keys():
            data['GPS_End_Lat'] = self.SetLatGeoCodeModel
        if 'GPS_End_Lon' in data.keys():            
            data['GPS_End_Lon'] = self.SetLonGeoCodeModel
        if 'bcBeTripDistanceKm' in data.keys():
            data['bcBeTripDistanceKm'] = self.GetDistanceKmGeoCodeModel
        if 'SEG_X_POS_LAT' in data.keys():
            data['SEG_X_POS_LAT'] = self.SetLatGeoCodeModel
        if 'SEG_X_POS_LON' in data.keys():
            data['SEG_X_POS_LON'] = self.SetLonGeoCodeModel    
        return data

    def calculateDay(self,name,mu,num,interval):
            now=0.
            result=[]
            #print "mu is ",mu
            #print "name is ",name
            for i in range(num):    
                #print "Event %d"%i
                nexttime=generateEventTimeWithinDay(mu,minVal=now)
                if (nexttime is None):
                    #print "End of the day. Event is not possible"
                    continue
                time_to_happen_in_seconds= int(nexttime)
                #print "event should happen in  ", time_to_happen_in_seconds, " real seconds"
                #print "event should happen in  ", time_to_happen_in_seconds/60./60., " real hours"                
                result.append(time_to_happen_in_seconds)
                now +=time_to_happen_in_seconds 
            toreturn={name:{
                    #"name":name,
                    "numEvt":num,
                    "timeline":histogram(result,0.,60.*60.*24.,1)[0]
                }}
            #print toreturn
            return toreturn


    def getTimeStamps(self,mu,num):
        now=0.
        for i in range(num):
            nexttime=generateEventTimeWithinDay(mu,minVal=now)
            if (nexttime is None):
                return
            time_to_happen_in_seconds= int(nexttime)
            now +=time_to_happen_in_seconds
            yield time_to_happen_in_seconds
    
        
    #sortedset=banyan.SortedSet([(float,string)])
    # sortedset is mutable!
    def calculateDay_v2(self,name,mu,num,sortedset):
            for timestamp in self.getTimeStamps(mu,num):
                sortedset.add((timestamp,name))
    
    def predictDay_v2(self,num_of_workers,sortedset):
        events= EventProducer(self.events)
        print "Car %s with id %d started predicting at the day %d in %s"%(self.idnum,self.counter,self.day_simulation,current_thread().name)
        for event in events:
            data= DataTypeProducer(self.data)
            data=next((x for x in data if x.namedata == event.name), None)
            if (data):
                numEvt=event.generate()
#                print "data of the type %s will be generated %d times"%(data.namedata,numEvt)
                self.calculateDay_v2(event.name,event.mu,numEvt,sortedset)
                del data           
        del events


    def simulateDay_v2(self,eventType,timestamp,_bin,numSim=0):        
        global lock
        self.day_of_week = self.today.weekday()
        print "Simulation of Car %d %s at the day %d in the %s"%(self.couter,self.idnum,self.day_simulation,current_thread().name)
#        print "for the _bin",_bin
        data= DataTypeProducer(self.data)
        data=next((x for x in data if x.namedata == eventType), None)
#        print "data of the type %s will be generated %d times"%(data.namedata,1)
        if (data):
            datajson={"Event":eventType}
            # fix the generators to the car specific ones!
            if eventType in ['Fahrtenhistorie','Steckhistorie']:
                self.GeoCodeModel()
                data = self.ReplaceGenerators(data)
            for key in data.keys():
                if '_' != key[0]: # if it is not the internal property
                    try:
                        #print "processing key ",key,data[key]
                        random_val=data[key]()
                        datajson.update({key:str(random_val[0])})
                    except NotImplementedError:
                        random_val=(np.inf,-1)
                        datajson.update({key:"-1"})
            try: 
                lock.acquire()
                StaticEvent.eventid+=1
                eventid=StaticEvent.eventid
                lock.release()
                #with ext_db.atomic():
                with ext_db.execution_context() as ctx:
                    if (multicolumn):
                        dyntable.create(uuid=self.idnum,timestamp=VirtualDateTime(_bin), weekday=self.day_of_week, daysim=self.day_simulation,secondbin=str(_bin),sim=numSim,eventid=eventid,**datajson)
                    else:
                        dyntable.create(uuid=self.idnum,timestamp=VirtualDateTime(_bin), weekday=self.day_of_week, daysim=self.day_simulation, eventdata=datajson,secondbin=str(_bin),sim=numSim,eventid=eventid)
                    DatabaseManager.successed.send(eventid)
                    #Cars.create(uuid=self.idnum,timestamp=timestamp, weekday=self.day_of_week, daysim=self.day_simulation, eventdata=datajson,secondbin=str(_bin))
            except Exception as e:
                print ">>> Exception in %s ext_db.execution_context-->"%current_thread().name, str(e)
                mylogging.setVerbosity('ERROR')('Event %d EVENT_RECORD_PROBLEM'%eventid)
                DatabaseManager.failed.send(eventid)
            #print datajson
            del data

    
    #@profile
    def predictDay(self,num_of_workers):
        events= EventProducer(self.events)
        #events = EventList
        #events = self.events

        '''
        def calculateDay(name,mu,num,interval):
            now=0.
            result=[]
            #print "mu is ",mu
            #print "name is ",name
            for i in range(num):    
                #print "Event %d"%i
                nexttime=generateEventTimeWithinDay(mu,minVal=now)
                if (nexttime is None):
                    #print "End of the day. Event is not possible"
                    continue
                time_to_happen_in_seconds= int(nexttime)
                #print "event should happen in  ", time_to_happen_in_seconds, " real seconds"
                #print "event should happen in  ", time_to_happen_in_seconds/60./60., " real hours"                
                result.append(time_to_happen_in_seconds)
                now +=time_to_happen_in_seconds 
            toreturn={name:{
                    #"name":name,
                    "numEvt":num,
                    "timeline":histogram(result,0.,60.*60.*24.,1)[0]
                }}
            #print toreturn
            return toreturn
        '''
        
        # avoid threading in cars
        #pool = ThreadPool(num_of_workers)
        result=[]
        print "Car %s with id %d started predicting"%(self.idnum,self.counter)
        for event in events:
            data= DataTypeProducer(self.data)
            data=next((x for x in data if x.namedata == event.name), None)
            if (data):
                numEvt=event.generate()
#                print "numEvt is ",numEvt
                # avoid threading in cars
                #pool.add_task(calculateDay,event.name,event.mu,numEvt,1)
                #result.append(calculateDay(event.name,event.mu,numEvt,1))
                result.append(self.calculateDay(event.name,event.mu,numEvt,1))
            del data
           
        #print "waiting for predictions"         
        
        # avoid threading in cars
        #pool.wait_completion()
        #print "Done"         
        # get result
        
        # avoid threading in cars
        #result=[res for res in pool.get_result()]
        del events
        return result

    def simulateDay(self,eventType,timestamp,_bin):
        self.day_of_week = self.today.weekday()
        print "Car %s at the day %d in the %s simulates bin %d"%(self.idnum,self.day_simulation,current_thread().name,_bin)
        data= DataTypeProducer(self.data)
        data=next((x for x in data if x.namedata == eventType), None)
        if (data):
            datajson={"Event":eventType}
            # fix the generators to the car specific ones!
            if eventType in ['Fahrtenhistorie','Steckhistorie']:
                self.GeoCodeModel()
                data = self.ReplaceGenerators(data)
            for key in data.keys():
                if '_' != key[0]: # if it is not the internal property
                    try:
                        #print "processing key ",key,data[key]
                        random_val=data[key]()
                        datajson.update({key:str(random_val[0])})
                    except NotImplementedError:
                        random_val=(np.inf,-1)
                        datajson.update({key:"-1"})
            try: 
                with ext_db.atomic(): 
                    if (multicolumn):
                             dyntable.create(uuid=self.idnum,timestamp=VirtualDateTime(_bin), weekday=self.day_of_week, daysim=self.day_simulation,secondbin=str(_bin),**datajson)
                    else:
                             dyntable.create(uuid=self.idnum,timestamp=VirtualDateTime(_bin), weekday=self.day_of_week, daysim=self.day_simulation, eventdata=datajson,secondbin=str(_bin))
                    #Cars.create(uuid=self.idnum,timestamp=timestamp, weekday=self.day_of_week, daysim=self.day_simulation, eventdata=datajson,secondbin=str(_bin))
            except Exception as e:
                print ">>> Exception in %s ext_db.execution_context-->"%current_thread().name, str(e)
                mylogging.setVerbosity('ERROR')('EVENT_RECORD_PROBLEM')
                print(traceback.format_exc())

            #print datajson
        # will be changed in other function
        #self.today += datetime.timedelta(days=1)
        #self.day_simulation+=1
        del data

    def updateDates(self):
        self.today += datetime.timedelta(days=1)
        self.day_simulation+=1

    
    def simulate(self,numSim=0):        
        global lock
        events= EventProducer(self.events)               
        self.day_of_week = self.today.weekday()
        #print self.day_of_week 
        #print self.today
        #print self.day_simulation
        print "Sim %d Car %d %s at the day %d in the %s"%(numSim,self.counter,self.idnum,self.day_simulation,current_thread().name)
        for event in events:
            data= DataTypeProducer(self.data)
            data=next((x for x in data if x.namedata == event.name), None)
            if (data):
                #ext_db.connect()
                numEvt=event.generate()
                #print "data of the type %s will be generated %d times"%(data.namedata,numEvt)
		now=0.
                for i in range(numEvt):# for each event of the particular type, generate synthetic data
                    #print "generating %d event"%i
                    ## calculation of the real timestamps here
                    nexttime=generateEventTimeWithinDay(event.mu,minVal=now)
                    if (nexttime is None):
                        continue
                    time_to_happen_in_seconds= int(nexttime)
                    now +=time_to_happen_in_seconds
                    _bin=getSecondBin(now)
                    datajson={"Event":event.name}
                    if event.name in ['Fahrtenhistorie','Steckhistorie']:
                        self.GeoCodeModel()
                        data = self.ReplaceGenerators(data)
                    for key in data.keys():
                        if '_' != key[0]: # if it is not the internal property
                            try:
                                random_val=data[key]()
                                datajson.update({key:str(random_val[0])})
                            except NotImplementedError:
                                random_val=(np.inf,-1)
                                datajson.update({key:"-1"})
                            #print key,random_val # it should  a message to send to the master 
                    #while True:
                    try:
                        lock.acquire()
                        StaticEvent.eventid+=1
                        eventid=StaticEvent.eventid
                        lock.release()
                        #print "eventdata-->", datajson
                        #with ext_db.atomic():
                        with ext_db.execution_context() as ctx:
                            #Cars.create(uuid=self.idnum,timestamp=VirtualDateTime(_bin), weekday=self.day_of_week, daysim=self.day_simulation, eventdata=datajson,secondbin=str(_bin))
                            if (multicolumn):
                             dyntable.create(uuid=self.idnum,timestamp=VirtualDateTime(_bin), weekday=self.day_of_week, daysim=self.day_simulation,secondbin=str(_bin),sim=numSim,eventid=eventid,**datajson)
                            else:
                             dyntable.create(uuid=self.idnum,timestamp=VirtualDateTime(_bin), weekday=self.day_of_week, daysim=self.day_simulation, eventdata=datajson,secondbin=str(_bin),sim=numSim,eventid=eventid)
#                            print "!!! %s  commiting finished"%current_thread().name
                            DatabaseManager.successed.send(eventid)
                            #break
                    except Exception as e:
                            print ">>> Exception in %s ext_db.execution_context-->"%current_thread().name, str(e)
                            print(traceback.format_exc())
                            mylogging.setVerbosity('ERROR')('Event %d EVENT_RECORD_PROBLEM'%eventid)
                            DatabaseManager.failed.send(eventid)

#                if not ext_db.is_closed():
#                    ext_db.close()
                
        self.today += datetime.timedelta(days=1)
        self.day_simulation+=1
        #raise NotImplementedError
    
    # to test generators 
    def test(self):
        from itertools import tee
        #_,eventiter=tee(self.events)
        #eventiter=self.events
        #for event in eventiter:        
        events= EventProducer(self.events)       
        for event in events:
            print "data of the type %s will be generated"%event.name
            print  [event.generate() for i in range(30)]
          
                
