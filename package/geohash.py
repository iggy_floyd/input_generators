#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Geohash calculations
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   geohash.py
#

'''
	Geohash calculations

'''

__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::



import math
from math import log10


# Geohash Alphabet
# ------------------------
#  Note: the alphabet in geohash differs from the common base32 alphabet described in IETF RFC 4648 (http://tools.ietf.org/html/rfc4648)
#

__base32 = '0123456789bcdefghjkmnpqrstuvwxyz'
__decodemap = dict()
for i in range(len(__base32)):
    __decodemap[__base32[i]] = i



# Grid precision
# ------------------------
# https://github.com/wdm0006/pygeohash/tree/master/pygeohash
# the distance between geohashes based on matching characters, in meters.
#

_PRECISION = {
    0: 20000000,
    1: 5003530,
    2: 625441,
    3: 123264,
    4: 19545,
    5: 3803,
    6: 610,
    7: 118,
    8: 19,
    9: 3.71,
    10: 0.6,
}


# ``encode``
# ------------------------
# Encode a position given in float arguments latitude, longitude to a geohash which will have the character count precision.
#

def encode(latitude, longitude, precision=12):
    """
    Encode a position given in float arguments latitude, longitude to
    a geohash which will have the character count precision.
    """
    lat_interval = (-90.0, 90.0)
    lon_interval = (-180.0, 180.0)
    geohash = []
    bits = [16, 8, 4, 2, 1]
    bit = 0
    ch = 0
    even = True
    
    while len(geohash) < precision:
        if even:
            mid = (lon_interval[0] + lon_interval[1]) / 2
            if longitude > mid:
                ch |= bits[bit]
                #print "ch=","{0:b}".format(ch)
                lon_interval = (mid, lon_interval[1])
            else:
                lon_interval = (lon_interval[0], mid)
        else:
            mid = (lat_interval[0] + lat_interval[1]) / 2
            if latitude > mid:
                ch |= bits[bit]
                lat_interval = (mid, lat_interval[1])
            else:
                lat_interval = (lat_interval[0], mid)
        even = not even
        if bit < 4:
            bit += 1
        else:
            geohash += __base32[ch]
            bit = 0
            ch = 0
    return ''.join(geohash)


# ``decode_exactly``
# ------------------------
# Decode the geohash to its exact values, including the error
# margins of the result.  Returns four float values: latitude,
# longitude, the plus/minus error for latitude (as a positive
# number) and the plus/minus error for longitude (as a positive
# number).

def decode_exactly(geohash):
    """
    Decode the geohash to its exact values, including the error
    margins of the result.  Returns four float values: latitude,
    longitude, the plus/minus error for latitude (as a positive
    number) and the plus/minus error for longitude (as a positive
    number).
    """
    lat_interval, lon_interval = (-90.0, 90.0), (-180.0, 180.0)
    lat_err, lon_err = 90.0, 180.0
    is_even = True
    for c in geohash:
        cd = __decodemap[c]
        for mask in [16, 8, 4, 2, 1]:
            if is_even: # adds longitude info
                lon_err /= 2
                if cd & mask:
                    lon_interval = ((lon_interval[0]+lon_interval[1])/2, lon_interval[1])
                else:
                    lon_interval = (lon_interval[0], (lon_interval[0]+lon_interval[1])/2)
            else:      # adds latitude info
                lat_err /= 2
                if cd & mask:
                    lat_interval = ((lat_interval[0]+lat_interval[1])/2, lat_interval[1])
                else:
                    lat_interval = (lat_interval[0], (lat_interval[0]+lat_interval[1])/2)
            is_even = not is_even
    lat = (lat_interval[0] + lat_interval[1]) / 2
    lon = (lon_interval[0] + lon_interval[1]) / 2
    return lat, lon, lat_err, lon_err


# ``decode``
# ------------------------
# Decode geohash, returning two float with latitude and longitude
# containing only relevant digits and with trailing zeroes removed.

def decode(geohash):
    """
    Decode geohash, returning two float with latitude and longitude
    containing only relevant digits and with trailing zeroes removed.
    """
    lat, lon, lat_err, lon_err = decode_exactly(geohash)
    # Format to the number of decimals that are known
    #print max(1, int(round(-log10(lat_err)))) - 1
    #print max(1, int(round(-log10(lat_err)))) - 1
    lats = "%.*f" % (max(1, int(round(-log10(lat_err)))) - 1, lat)
    lons = "%.*f" % (max(1, int(round(-log10(lon_err)))) - 1, lon)
    #print lats,lons
    if '.' in lats: lats = lats.rstrip('0')
    if '.' in lons: lons = lons.rstrip('0')
    """{'lat':__, 'lon':__, 'h':__, 'w':__}"""
    return {'lat':float(lats),'lon':float(lons),'w':2.*float(lon_err),'h':2.*float(lat_err)}         
    #return float(lats), float(lons),float(lat_err),float(lon_err)


# ``neighbors``
# ------------------------
# returns neighbors of the geohash cell
#

def neighbors(geohash,precision=12):   
    lat, lon, lat_err, lon_err = decode_exactly(geohash)
    w = 2.*float(lon_err)
    h= 2.*float(lat_err)
    #_decode=decode2(geohash)
    
    #encode2(latitude, longitude, precision=12)
    #lat_interval, lon_interval = (-90.0, 90.0), (-180.0, 180.0)
    #lat_err, lon_err = 90.0, 180.0
    return {
       'n': encode(lat+h,lon,precision),
       'nw': encode(lat+h,lon-w,precision),
       'w': encode(lat,lon-w,precision),
       'sw': encode(lat-h,lon-w,precision),
       's': encode(lat-h,lon,precision),
       'se': encode(lat-h,lon+w,precision),
       'e': encode(lat,lon+w,precision),
        'ne': encode(lat+h,lon+w,precision)
    }


# ``BoundBox``
# ------------------------
# returns a Bound Box of the set of geohash cells, for example for 
#
# Berlin:
# Location (lat/lon): 52.506809, 13.42487
# NE 52.675499, 13.76134
# SW 52.33812, 13.0884
# 

def BoundBox(geohashes=[]):
    _bbox={
        'NE':(None,None),
        'SW':(None,None)
    }
    
    if len(geohashes)==0 : 
        return _bbox
    elif len(geohashes) == 1:
        _decode=decode(geohashes[0])
        _bbox['NE']=(_decode['lat']+_decode['h']/2.,_decode['lon']+_decode['w']/2.)
        _bbox['SW']=(_decode['lat']-_decode['h']/2.,_decode['lon']-_decode['w']/2.)

    else:
        geohashes= list(set(geohashes)) # remove  duplications 
        _tmp=[ dict({'name':gh}.items() + {'props':decode_exactly(gh)}.items()) for gh in geohashes]
        _rightmost=sorted(_tmp,key= lambda x: x['props'][1]+x['props'][3],reverse=True)
        _lefmost=sorted(_tmp,key= lambda x: x['props'][1]-x['props'][3],reverse=False)
        _topmost=sorted(_tmp,key= lambda x: x['props'][0]+x['props'][2],reverse=True)
        _bottommost=sorted(_tmp,key= lambda x: x['props'][0]-x['props'][2],reverse=False)
        _maxlats=_topmost[0]['props']
        _maxlons=_rightmost[0]['props']
        _minlats=_bottommost[0]['props']
        _minlons=_lefmost[0]['props']
        _bbox['NE']=(_maxlats[0]+_maxlats[2],_maxlons[1]+_maxlons[3])
        _bbox['SW']=(_minlats[0]-_minlats[2],_minlons[1]-_minlons[3])
        
        
    return _bbox

# ``metropoly``
# ------------------------
# creates the metropoly's geohashes from the bounding box

def metropoly(latitude,longitude,bbox,precision=12):
    
    # code of the center of the metropoly
    code=encode(latitude,longitude,precision)
    # lat/lon
    NE=bbox['NE'] # the top-right point of the BBOX
    SW=bbox['SW'] # the bottom-left point of the BBOX
    width=NE[1] - SW[1]
    assert width>0, "Wrong Input BBOX: lon(NE)<lon(SW)"
    height=NE[0] - SW[0]
    assert height>0, "Wrong Input BBOX: lat(NE)<lat(SW)"
    _bbox = BoundBox([code])
    _NE =_bbox['NE']
    _SW =_bbox['SW']
    _width=_NE[1] - _SW[1]
    assert _width>0, "Wrong Obtained BBOX for center: lon(NE)<lon(SW)"
    _height=_NE[0] - _SW[0]
    assert _height>0, "Wrong Obtained BBOX for center: lat(NE)<lat(SW)"
    if (width<_width) and (height<_height):
        return [code]
    parts=[]
    stack=[code]
    while len(stack):
        elem = stack.pop(0)
        if elem in parts:
            continue
        parts.append(elem)
        # find all neighbors around the 'code'
        codes=neighbors(elem,precision)        
        # transform it to the list
        codes=codes.values()
        # build list of the interested objects
        _tmp=[ dict({'name':gh}.items() + {'props':decode_exactly(gh)}.items()) for gh in codes]
        # filtering on longitute
        _filtered=filter(lambda x: (x['props'][1]-x['props'][3] >= SW[1]) and (x['props'][1]+x['props'][3] <= NE[1]),_tmp) 
        # filtering on latitude
        _filtered=filter(lambda x: (x['props'][0]-x['props'][2] >= SW[0]) and (x['props'][0]+x['props'][2] <= NE[0]),_filtered) 
        filtered=[ cand['name'] for cand in _filtered]
        for candidate in filtered:
            if not(candidate in stack):
                stack.append(candidate)
    return parts


# ``geohash_approximate_distance``
# ----------------------------------------
# Returns the approximate great-circle distance between two geohashes in meters.

def geohash_approximate_distance(geohash_1, geohash_2, check_validity=False):
    """
    Returns the approximate great-circle distance between two geohashes in meters.
    :param geohash_1:
    :param geohash_2:
    :return:
    """

    if check_validity:
        if len([x for x in geohash_1 if x in __base32]) != len(geohash_1):
            raise ValueError('Geohash 1: %s is not a valid geohash' % (geohash_1, ))

        if len([x for x in geohash_2 if x in __base32]) != len(geohash_2):
            raise ValueError('Geohash 2: %s is not a valid geohash' % (geohash_2, ))

    # normalize the geohashes to the length of the shortest
    len_1 = len(geohash_1)
    len_2 = len(geohash_2)
    if len_1 > len_2:
        geohash_1 = geohash_1[:len_2]
    elif len_2 > len_1:
        geohash_2 = geohash_2[:len_1]

    # find how many leading characters are matching
    matching = 0
    for g1, g2 in zip(geohash_1, geohash_2):
        if g1 == g2:
            matching += 1
        else:
            break

    matching += 1
    # we only have precision metrics up to 10 characters
    if matching > 10:
        matching = 10
    return _PRECISION[matching]

# ``geohash_haversine_distance``
# ----------------------------------------
# converts the geohashes to lat/lon and then calculates the haversine great circle distance in meters.

def geohash_haversine_distance(geohash_1, geohash_2):
    """
    converts the geohashes to lat/lon and then calculates the haversine great circle distance in meters.
    :param geohash_1:
    :param geohash_2:
    :return:
    """

    #print  decode_exactly(geohash_1) 
    #print  decode_exactly(geohash_2) 
    #lat_1, lon_1 = decode2(geohash_1)['lat'],decode2(geohash_1)['lon']
    #lat_2, lon_2 = decode2(geohash_2)['lat'],decode2(geohash_2)['lon']
    lat_1, lon_1,_,_ = decode_exactly(geohash_1)
    lat_2, lon_2,_,_ = decode_exactly(geohash_2)

    #print lat_1, lon_1
    #print lat_2, lon_2
    
    R = 6371000.
    phi_1 = math.radians(lat_1)
    phi_2 = math.radians(lat_2)

    delta_phi = math.radians(lat_2-lat_1)
    delta_lambda = math.radians(lon_2-lon_1)

    #print phi_1,phi_2
    #print delta_phi,delta_lambda
    a = math.sin(delta_phi/2.0) * math.sin(delta_phi/2.0) + math.cos(phi_1) * math.cos(phi_2) * math.sin(delta_lambda/2) *math.sin(delta_lambda/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))

    #print a
    #print c
    return R * c


# ``BoundingBox``
# ----------------------------------------
# a class representing the Bounding Box of a region


class BoundingBox(object):
    def __init__(self, *args, **kwargs):
        self.lat_min = None
        self.lon_min = None
        self.lat_max = None
        self.lon_max = None

# ``get_bounding_box``
# ----------------------------------------
# a function to build a bounding box from a circle around some position

def get_bounding_box(latitude_in_degrees, longitude_in_degrees, half_side_in_miles):
    assert half_side_in_miles > 0
    assert latitude_in_degrees >= -90.0 and latitude_in_degrees  <= 90.0
    assert longitude_in_degrees >= -180.0 and longitude_in_degrees <= 180.0

    half_side_in_km = float(half_side_in_miles) #* 1.609344
    lat = math.radians(latitude_in_degrees)
    lon = math.radians(longitude_in_degrees)

    radius  = 6371.
    # Radius of the parallel at given latitude
    parallel_radius = radius*math.cos(lat)

    lat_min = lat - half_side_in_km/radius
    lat_max = lat + half_side_in_km/radius
    lon_min = lon - half_side_in_km/parallel_radius
    lon_max = lon + half_side_in_km/parallel_radius
    rad2deg = math.degrees

    box = BoundingBox()
    box.lat_min = rad2deg(lat_min)
    box.lon_min = rad2deg(lon_min)
    box.lat_max = rad2deg(lat_max)
    box.lon_max = rad2deg(lon_max)

    return (box)

# ``BBoxInNESW2``
# ----------------------------------------
# a function to return a bbox in NE, SW
def BBoxInNESW2(distance, latittude, longitude):    
    box=get_bounding_box(latittude,longitude,distance)
    return (box.lat_max,box.lon_max),(box.lat_min,box.lon_min)


