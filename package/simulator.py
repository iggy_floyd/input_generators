#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Simulator
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   simulator.py
#

'''
	The Simulator class

'''


__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::


from .geohash import * 
from .car import Car
from .utils import makeUUID
from .events import event_list
from .datatypes import datatypes_list
import random
from .multithreads import *
from threading import Thread, current_thread
import time
from .utils import *
from banyan import SortedSet


# Simulator class
# --------------------
# The class ``Simulator`` steers the simulation process.
# 

class Simulator(object):
    
    def __init__(self,config):
        self.config=config
        self.cars=[]
        self.metropolis=None
        poolsize=config["ThreadPoolSize"]
        #if not config["RealTimeSimulation"]:
        #    if poolsize>50:
        #        poolsize=50
        #self.pool =  ThreadPool(poolsize,config["RealTimeSimulation"])
        self.pool =  ThreadPool(poolsize,False)
        #self.pool =  ThreadPool(90,config["RealTimeSimulation"])
        self.prediction_pool = ThreadPool(500,config["RealTimeSimulation"],True)
        latitude,longitude=self.config["metropolyLocation"]
        bbox=self.config["metropolyBBOX"]
        precision=self.config["metropolyPrecision"]
        self.virtualTimeFactor=self.config["virtualTimeFactor"]
        self.metropolis = metropoly(latitude,longitude,bbox,precision)
        print "producing cars..."
        self.produce(config["numCars"])
        print "producing cars... done"
        
    def produce(self,numCars):
        for _ in range(numCars):
            code = random.choice(self.metropolis)
            self.cars.append( Car(makeUUID(),eventList=event_list, dataList=datatypes_list,geohash=code,metropolis=self.metropolis) )
    
    

    def predictDay(self):
        for car in self.cars:
#            self.pool.add_task(car.predictDay,2)
            self.prediction_pool.add_task(car.predictDay,2)
#        print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!waiting in prediction days"    
        self.prediction_pool.wait_completion()
#        print ">>>>>>>>>>>>>>>>>>>><waiting in prediction days ...done"    
        # ready    
        eventsPredicted=[res for res in self.prediction_pool.get_result()]
        self.prediction_pool.clear_result()
        # combine and return final result
        print "merging predictions"
        merged=self.MergePredictions(eventsPredicted)
        #del eventsPredicted
        return merged

    @time_usage
    def BinSimulation(self,first,_bin):
#        print "processing bin ", _bin
        to_be_simulated = EventBinHappen(first,_bin)
#        print to_be_simulated
        for key in to_be_simulated.keys():
#            if to_be_simulated[key]>0:
#                print "key,bin,num",key,_bin,to_be_simulated
            for _ in range(to_be_simulated[key]):
                car = random.choice(self.cars)
#                print "car taken %s"%car.idnum
                self.pool.add_task(car.simulateDay,key,VirtualDateTime(_bin),_bin) # should be provided!
        # wait for all cars in the bin
#        print "waiting to be completed"
        self.pool.wait_completion()
        self.pool.clear_result()


    @time_usage
    def BinSimulation_v2(self,eventtype,timestamp,numSim=0):
        car = random.choice(self.cars)        
        print "Taken Car %s with id %d "%(car.idnum,car.counter)
        car.simulateDay_v2(eventtype,VirtualDateTime(timestamp),timestamp,numSim)

#	self.pool.add_task(car.simulateDay_v2,eventtype,VirtualDateTime(timestamp),timestamp)
#        self.pool.wait_completion()
#        self.pool.clear_result()
        return
        
    def simulateDay_v2(self,sortedset,numSim=0):
        # simulate the first timestamp
        prev_timestamp = 0
        while sortedset:
            elem=sortedset.pop()
            timestamp = elem[0]
            eventtype = elem[1]
#            print "timestamp=",timestamp
#            print "eventtype=",eventtype
            time_usage=self.BinSimulation_v2(eventtype,timestamp,numSim)
            time_to_wait=timestamp-time_usage-prev_timestamp
            if time_to_wait>0.:
#                print "sleeping for %f"%time_to_wait
                print "real sleeping for %f"%(time_to_wait*self.virtualTimeFactor)
                time.sleep(time_to_wait*self.virtualTimeFactor)
            prev_timestamp = timestamp
        self.updateDates()    
        return         

    def predictDay_v2(self,sortedset):
        for car in self.cars:
            self.pool.add_task(car.predictDay_v2,2,sortedset)
        print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!waiting in prediction days"    
        self.pool.wait_completion()
        print ">>>>>>>>>>>>>>>>>>>><waiting in prediction days ...done"    
        # ready            
        return 
    
    
    def runRealTime_v2(self):
        for _ in range(self.config["numSimulations"]):
            print "!!!New day"
            sortedset = SortedSet([])
            print "Start predicting day"
            self.predictDay_v2(sortedset)
            print "obtained sortedset",sortedset
            print "start simulating day"
            self.simulateDay_v2(sortedset)
            print "deleting sortedSet"
            del sortedset


    def updateDates(self):
        for car in self.cars:
            self.pool.add_task(car.updateDates) # should be provided!
        self.pool.wait_completion()
        self.pool.clear_result()
        

        
    
    def MergePredictions(self,eventsPredicted):        
#        print "In MergePredictions"
        chunks = chunkify(eventsPredicted,self.pool.num_threads)
        #print "chuncks are ", chunks
        #print "chunks done",chunks
        def task(predictions):        
            #print "In task",predictions
            #if len(predictions)==0:
                #print "In task ", predictions
            firstpredict=MapToDict(predictions[0])
            for i,predict in enumerate(predictions):
                if i==0: continue
                predict = MapToDict(predict)
                for key in predict.keys():
                    firstpredict[key]=SumDicts(firstpredict[key],predict[key])
            return firstpredict
        for chunk in chunks:  
            #print "adding task in MergePredictions"
            self.prediction_pool.add_task(task,chunk)
        self.prediction_pool.wait_completion()
#        print "finished waiting in MergePredictions"
        merged = [res for res in self.prediction_pool.get_result()]
        self.prediction_pool.clear_result()
#        print "done"
        return merged    

    def simulateDay(self,merged):    
        first = merged[0]
        for i,second in enumerate(merged):
            if i==0: continue            
            for key in second.keys():
                first[key]=SumDicts(first[key],second[key])
        # get an example of the timeline
        key =first.keys()[0]
        timelinehisto=first[key]['timeline']
        # lets simulated all bins
#        print "we are ready to start bin simulations"
        for _bin in range(len(timelinehisto)):
        #for _bin in np.where(first['Fahrtenhistorie']['timeline']>0)[0]:
            # put delay of one second
            time_usage=self.BinSimulation(first,_bin)
            time_to_wait=1.0-time_usage
            if time_to_wait>0.:
 #               print "sleeping for %f"%time_to_wait
                time.sleep(time_to_wait*self.virtualTimeFactor)
        # fix a problem with one day increment
        self.updateDates()    
        return         



        
    def runRealTime(self):
        for _ in range(self.config["numSimulations"]):
#            print "Start predicting day"
            data=self.predictDay()
#            print "start simulating day"
#            self.simulateDay(data)
    
    def simulate(self,numSim=0):
        print "Starting simulations!"
        for car in self.cars:
            #def f():
            #    print "In process%s"%current_thread().name
            #g = lambda :(car.simulate(),f())
            self.pool.add_task(car.simulate,numSim)
            #self.pool.add_task(g)
        print "waiting in simulation"    
        self.pool.wait_completion()
        print "done"
        time.sleep(10)
        
            
    def runLoadBalance(self,numSim=0):
        for _ in range(self.config["numSimulations"]):
            self.simulate(numSim)
            
    def run(self):
        if self.config["RealTimeSimulation"]:
            print "realtime"
            #self.runRealTime()
            self.runRealTime_v2()
        else:
            print "LoadBalance simulation"
            self.runLoadBalance()
