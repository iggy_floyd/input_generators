#! /usr/bin/env python
# -*- coding: utf-8 -*-

import redis
redis_host = "redis"
redis_port = 6379
from banyan import SortedSet
import pickle


class User(object):
    def __init__(self,x):
        self.x =x


r = redis.StrictRedis(host=redis_host, port=redis_port, db=0)
t = SortedSet([(1, User(1)), (2, User(2)), (-2, User(-2))])
for a in t:
    print a[1].x
    

pickled_object = pickle.dumps(t)
r.set('test_me', pickled_object)
unpacked_object = pickle.loads(r.get('test_me'))
print t == unpacked_object    
for a in unpacked_object:
    print a[1].x
    
print r.delete('test_me')
