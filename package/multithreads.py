#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Worker Multi-threading Queue 
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   multithreads.py
#

'''
	The Worker Queue

'''


__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::

import sys
IS_PY2 = sys.version_info < (3, 0)

if IS_PY2:
    from Queue import Queue
else:
    from queue import Queue

from threading import Thread, current_thread
import time
import pqueue as pq
import cPickle
import zlib
import traceback
import sys
from .utils import makeUUID
from .logger import mylogging

# Worker class
# --------------------
# The class ``Worker`` get a a task from the queue and executes in a thread
# 

class Worker(Thread):
    """ Thread executing tasks from a given tasks queue """
    def __init__(self, tasks,result,store_results,compress=False):
        Thread.__init__(self)
        self.tasks = tasks
        self.result = result
        self.store_results=store_results
        self.compress=compress
        self.daemon = True
        self.start()

    def run(self):
        global result
        while True:
            func, args, kargs = self.tasks.get()
            try:
                #print func,args,kargs
                #print func(*args, **kargs)
                if (self.store_results):
                    if self.compress:
                      self.result.put(zlib.compress(cPickle.dumps(func(*args, **kargs)))) 
                    else:
                      self.result.put(func(*args, **kargs)) # get a lot of memory in case of inmemory queue
                else:
                    func(*args, **kargs)  
                #self.result.put(zlib.compress(cPickle.dumps(func(*args, **kargs))))  # too slow                              
            except Exception as e:
                # An exception happened in this thread
                print "Exception here-->",str(e)
                print(traceback.format_exc())
                mylogging.setVerbosity('ERROR')('WORKER_EXCEPTION')
            finally:
                # Mark this task as done, whether an exception happened or not
                self.tasks.task_done()

# ThreadPool class
# --------------------
# The class ``ThreadPool`` creates the queue and add tasks to it for further execution.
# 


class ThreadPool:
    """ Pool of threads consuming tasks from a queue """
    def __init__(self, num_threads,store_results=True,compress=False):
        self.tasks = Queue(num_threads)
        #self.result = Queue() # get a lot of memory
        self.result = pq.Queue("/tmp/"+makeUUID()) # slow!
        #self.result = pq.Queue("/package/"+makeUUID()) # slow!
        #self.result = pq.Queue("/tmp/test2") # slow!
        self.compress=compress
        
        self.num_threads = num_threads
        for _ in range(num_threads):
            Worker(self.tasks,self.result,store_results,compress)
            
    def add_task(self, func, *args, **kargs):
        """ Add a task to the queue """
        self.tasks.put((func, args, kargs))

    def map(self, func, args_list):
        """ Add a list of tasks to the queue """
        for args in args_list:
            self.add_task(func, args)

    def wait_completion(self):
        """ Wait for completion of all the tasks in the queue """
        self.tasks.join()
        
        
    # Check thread's return value    
    def get_result(self):
        #while not self.result.empty():
            #result = self.result.get()
            #result = cPickle.loads(zlib.decompress(self.result.get()))
        while self.result._qsize():
            result = self.result.get_nowait()
            if self.compress:
               yield cPickle.loads(zlib.decompress(result))
            else:
               yield result
            
    def clear_result(self):
        #with self.result.mutex:
        #    self.result.queue.clear()
        while self.result._qsize():
            result = self.result.get_nowait()
        
    
