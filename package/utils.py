#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Utilities
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   utils.py
# :Copyright: @ Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de
#


'''
    Utilities
'''

from __future__ import division

__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"



# Requirements
# ------------
#
# ::


import numpy as np
import pytz, datetime
from scipy.stats import truncnorm
import random
from .geohash import * 
import math
import random
import time


# ``get_nth_event``
# ------------------------
# a function to retrives the n-th element from a python generator object
# 

def get_nth_event(events,n):
    from itertools import islice
    return next(islice(events, n, n+1))



# ``generate_from_histogram``
# ----------------------------------------
# a function to randomly generate floating numbers from a histogram
# 


def generate_from_histogram(data,size=100,bins=None):
    
    bins = bins if bins else 50
    hist, bins = np.histogram(data, bins=bins)
    bin_midpoints = bins[:-1] + np.diff(bins)/2
    cdf = np.cumsum(hist)
    cdf = cdf / cdf[-1]
    values = np.random.rand(size)
    value_bins = np.searchsorted(cdf, values)
    random_from_cdf = bin_midpoints[value_bins]
    return random_from_cdf


# ``RandomIntNumberSeqGenerator``
# ----------------------------------------
# a function to uniforlmy generate integer numbers from an interval
# 

def RandomIntNumberSeqGenerator(minval=1,maxval=6,lendigits=4):
    '''pseudo random generation of the seq of integerss'''
    
    code=np.random.random_integers(minval,maxval,lendigits)
    code = map(lambda x: str(x),code)
    return (''.join(code),len(code))


# ``RandomFloatNumberGenerator``
# ----------------------------------------
# a function to uniforlmy generate float numbers from an interval
# 

def RandomFloatNumberGenerator(minval=5.,maxval=100.,stepsize=0.01):
    '''pseudo random generation of the float32 numbers'''    
    choice=random.choice(range(int(float(maxval-minval)/stepsize)+1))
    value = minval + stepsize*choice
    return (value,4)


# ``RandomDatetimeInSecs``
# ----------------------------------------
# a function to  generate randomly timestamps in seconds in an interval
# 


# helper funtion to generate random timestamps
# http://stackoverflow.com/questions/553303/generate-a-random-date-between-two-other-dates
# http://stackoverflow.com/questions/100210/what-is-the-standard-way-to-add-n-seconds-to-datetime-time-in-python
# http://stackoverflow.com/questions/7852855/how-to-convert-a-python-datetime-object-to-seconds
def RandomDatetimeInSecs(start=datetime.datetime.utcnow(),deltadatetime = datetime.timedelta(days=1)):
    from random import randint
    import time 
    import calendar
    import datetime 
    
    #end = start+deltadatetime
    randomdate=start + datetime.timedelta(seconds=randint(0, int(deltadatetime.total_seconds())))
    return (int(time.mktime(randomdate.timetuple())),8)

# ``DatetimeInSecs``
# ----------------------------------------
# a function to transform datetime objects to timestamps in seconds 
# 

def DatetimeInSecs(start=datetime.datetime.utcnow()):    
    import time 
    import calendar
    import datetime         
    randomdate=start
    return (int(time.mktime(randomdate.timetuple())),8)


# ``local_to_utc``
# ----------------------------------------
# a function to transform Local datetime objects to UTC datetime objects 
# 


def local_to_utc(t,tz="Europe/Berlin"):
    local_zone = pytz.timezone (tz)
    local_dt = local_zone.localize(t, is_dst=None)
    return  local_dt.astimezone (pytz.utc)



# ``RandomTrunctatedNormalGenerator``
# ----------------------------------------
# a function to generate floats from Trunctated Normal distribution

#example of the triuncated normal distribution http://kieranrcampbell.github.io/fast-vectorized-sampling/
def RandomTrunctatedNormalGenerator(mu,sd,lower_clip,upper_clip):
    def truncnorm_rvs_wrapper(mu, sd, lower_clip, upper_clip):
        from scipy.stats import truncnorm
        return truncnorm.rvs((lower_clip - mu) / sd, (upper_clip-mu)/sd, mu, sd)
    truncnorm_rvs = np.vectorize(truncnorm_rvs_wrapper, otypes=[np.float])    
    #return (truncnorm_rvs(mu,sd,lower_clip,upper_clip),4)
    return truncnorm.rvs((lower_clip - mu) / sd, (upper_clip-mu)/sd, mu, sd)



# ``FVerteilung``
# ----------------------------------------
# a function to generate floats from the distribution in the the excel  xls file

# built from F-Norm Verteilung
# idea taken from http://stackoverflow.com/questions/4265988/generate-random-numbers-with-a-given-numerical-distribution
def FVerteilung(minval,maxval):
    pdf_original=[0.0016225865,0.0227906354,0.0670491538,0.1074358323,
                  0.1278136477,0.1284663744,0.1163612798,0.098487983,0.0796360466,0.0623998436,
                  0.0478388781,0.0361252915,0.0269993681,0.0200411875,0.0148130181,0.0109233448,
                  0.0080481622,0.0059313295,0.0043761564,0.0032344729]    
    pdf_inferred = pdf_original+[1.0-sum(pdf_original)]
    bins=np.linspace(float(minval), float(maxval), num=len(pdf_inferred))
    left_edge=np.random.choice(bins, p=pdf_inferred)
    width=bins[1]-bins[0]
    rand_width=width*np.random.rand()
    return min(float(maxval),left_edge+rand_width)


# ``place_a_car_to_cell``
# ----------------------------------------
# a function to randomly the cars at the different cells defined by geohash codes

def place_a_car_to_cell(codes,cars):
    assert len(codes) == len(cars), 'Wrong codes and cars lists'
    for i,code in enumerate(codes):
        cars[i].geohash=code
        cars[i].lat=decode_exactly(code)[0]
        cars[i].lon=decode_exactly(code)[1]

        
# ``makeUUID``
# ----------------------------------------
# a function to randomly  generate UUID

def makeUUID():
    import uuid
    return str(uuid.uuid4()).replace("-","")


# ``nextTime`` and ``nextTime_v2``
# ----------------------------------------
# a function to randomly  generate Poisson timestamps


def nextTime(rateParameter):
    return -math.log(1.0 - random.random()) / rateParameter

nextTime_v2 = lambda rateParameter: random.expovariate(rateParameter)


# ``generateEventTimeWithinDay`` 
# ----------------------------------------
# a function to randomly  generate Poisson timestamps within one day

        

def generateEventTimeWithinDay(mu,minVal=0.,maxVal=60.*60.*24.): # in seconds
        '''  mu for one day!'''
        rateParameter  = mu/(maxVal) # per seconds
        nexttime=minVal+nextTime_v2(rateParameter)
        return nexttime if  nexttime<maxVal else None
    


# ``histogram`` 
# ----------------------------------------
# a function to make histogram

def histogram(data,minval,maxval,interval):
    numsample=int(float(maxval-minval)/float(interval)+1)
    bins= np.linspace(minval,maxval,numsample)
    return np.histogram(data, bins=bins)    


# ``time_usage`` 
# ----------------------------------------
# a function decorator to estimate the usage time of the working function

def time_usage(func):
    def wrapper(*args, **kwargs):
        beg_ts = time.time()
        func(*args, **kwargs)
        end_ts = time.time()
        #print("elapsed time: %f" % (end_ts - beg_ts))
        return (end_ts - beg_ts)
    return wrapper


# ``VirtualDateTime`` 
# ----------------------------------------
# a function to transform datetime in seconds to a datetime objects 

def VirtualDateTime(seconds,year=2000,month=1,day=1):    
    import datetime
    #_datetime=datetime.datetime(year, month, day, 0, 0, 0)
    year=datetime.datetime.fromtimestamp(DatetimeInSecs()[0]).year
    month=datetime.datetime.fromtimestamp(DatetimeInSecs()[0]).month
    day=datetime.datetime.fromtimestamp(DatetimeInSecs()[0]).day
    _datetime=datetime.datetime(year, month, day, 0, 0, 0)
    return _datetime+datetime.timedelta(seconds=seconds)

# ``getSecondBin`` 
# ----------------------------------------
# a function to get a bin index from timestamp

def getSecondBin(seconds):
        numsample=int(float(60.*60.*24.-0.)/1.+1)
        bins= np.linspace(0.,60.*60.*24.,numsample)
        ind=np.digitize([seconds],bins, right=False)[0]
        return ind-1


# ``chunkify`` 
# ----------------------------------------
# a function to build a list of chunks of objects from the list of objects


def chunkify(lst,n):
    if len(lst)<n:
        n=len(lst)
    return [ lst[i::n] for i in xrange(n) ]

#def chunkify(l, n):
#    """
#    divide a list l into n equally sized chunks (except maybe the last one)
#    """
#    ll=[]
#    for i in range(n):
#        ll.append(l[i::n])
#    return ll



# ``MapToDict``, ``SumDicts``, ``EventBinHappen``
# ----------------------------------------------------
# helper functions 


def MapToDict(data):
    res={}
    for elem in data:
        for key in elem.keys():
            res.update({key:elem[key]})
    return res

def SumDicts(dic1,dic2):
    for key in dic1.keys():
        dic1[key]+=dic2[key]
    return dic1

def EventBinHappen(data,_bin):
    result={}
    for key in data.keys():
        result.update({key:data[key]['timeline'][_bin]})
    return result
