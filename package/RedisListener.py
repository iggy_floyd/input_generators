#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Listener of events in the Redis Database
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   simulator.py
#

'''
	The Listener class

'''


__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"

# Requirements
# ------------
#
# ::


import redis
import threading
import time

# Redis connection
# -------------------
#
# ::

redis_host = "redis"
redis_port = 6379


# Listener class
# --------------------
# The class ``Listener`` listens to events and executes a ``work`` as a daemon.
# 


class Listener(threading.Thread):
    do_run=True
    def __init__(self, r, channels,daemon=True):
        threading.Thread.__init__(self)
        self.redis = r
        self.daemon = daemon
        self.pubsub = self.redis.pubsub()
        self.pubsub.subscribe(channels)
        self.job=None
    
    def work(self, item):
        print item['channel'], ":", item['data']
    
    def run(self):
        for item in self.pubsub.listen():
            print "status of the job ", Listener.do_run
            term=0
            try:
             term=self.redis.get('terminate')
             print "term is ",term,type(term)
            except Exception as e:
              print "Exception in Listner",str(e)
              pass
            if term=="1": 
                print  "breaking..."
                self.redis.set('terminate',0)
                break
            if  not Listener.do_run:
                print  "breaking..."
                break
            if (item['data'] == "KILL") or ( not getattr(self, "do_run", True) ) or ( not Listener.do_run):
                self.pubsub.unsubscribe()
                print self, "unsubscribed and finished"
                Listener.do_run=True    
                break
            else:
                self.work(item)
    def wait(self):
        self.join()

if __name__ == "__main__":
    r= redis.StrictRedis(host=redis_host, port=redis_port, db=0)
    client = Listener(r, ['test'])
    client.start()
    #time.sleep(1)
    r.publish('test', 'this will reach the listener')
    #time.sleep(5)
    r.publish('test', 'this will reach the listener')
    #time.sleep(10)
    r.publish('fail', 'this will not')
    Listener.do_run=False
    r.publish('test', 'this will reach the listener')
    #r.publish('test', 'KILL')
    client.wait()
