#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Car Motion Generators
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   motion.py
# :Copyright: @ Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de
#

'''
	Car Motion Generators

'''

__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::

from .geohash import *
import random


# ``RandomMotion``
# --------------------
# The ``basic`` function to randomly generate movement from one geohash cell to another one.
# 


def RandomMotion(code,metropolis):
    ''' returns random cell from metropolis, distance between geohash 'code' and a new code '''
    new_code=None
    while True:
        new_code=random.choice(metropolis)
        if new_code != code:
            break
    distance=geohash_haversine_distance(code,new_code)
    return(new_code,distance)


# ``RandomXXXGenerator``
# --------------------------
# generator functions to extract some information, like Geohash code, Distance etc, from a result of the ``RandomMotion``
# 


RandomCellGenerator = lambda code,metropolis: (setattr(RandomCellGenerator,"val",RandomMotion(code,metropolis)), getattr(RandomCellGenerator,"val"))[1]

RandomGeohashGenerator = lambda code,metropolis: RandomMotion(code,metropolis)[0] if not hasattr(RandomCellGenerator,"val") else RandomCellGenerator.val[0]

RandomDistanceGenerator = lambda code,metropolis: RandomMotion(code,metropolis)[1] if not hasattr(RandomCellGenerator,"val") else RandomCellGenerator.val[1]

RandomLatLonGenerator = lambda code,metropolis: decode_exactly(RandomMotion(code,metropolis)[0])[:2] if not hasattr(RandomCellGenerator,"val") else decode_exactly(RandomCellGenerator.val[0])[:2]
