#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Model (Database Schema)
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   model.py
#

'''
	Model (Database Schema)

'''

__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::


from playhouse.postgres_ext import *
from playhouse.pool import PooledPostgresqlExtDatabase
from peewee import *
import peewee
import datetime
import os
import traceback
from .datatypes import *
from  .configurator import  Configurator

# Database connection
# ------------------------
#
# ::


DATABASE = {
        'NAME': os.environ.get('DB_NAME'),
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': os.environ.get('DB_PORT'),
}


#ext_db = PostgresqlExtDatabase(DATABASE['NAME'],host=DATABASE['HOST'],port=DATABASE['PORT'], user=DATABASE['USER'],password=DATABASE['PASSWORD'],threadlocals=True,register_hstore=True)
ext_db = PooledPostgresqlExtDatabase(database=DATABASE['NAME'],host=DATABASE['HOST'],port=DATABASE['PORT'], user=DATABASE['USER'],password=DATABASE['PASSWORD'],threadlocals=True,register_hstore=True,max_connections=900,stale_timeout=10)



# ``BaseModel``
# ------------------------
# The ``BaseModel`` class defines the meta data: a default connection to the database


class BaseModel(Model):
    class Meta:
        database = ext_db


# ``Cars``
# ------------------------
# The ``Cars`` class defines schema of the data storage in the Postgresql

def Factory(fields):
    class Cars(BaseModel):
    	uuid = CharField()
    	timestamp = DateTimeField()
    	weekday = IntegerField()
    	daysim = IntegerField()
    	secondbin=IntegerField()
        sim=IntegerField()
        eventid=IntegerField(null=True)
    	eventdata=HStoreField(null=True)

    for field in fields:
        peewee.CharField(null=True).add_to_class(Cars, field)
    return Cars


#class Cars(BaseModel):
#    uuid = CharField()
#    timestamp = DateTimeField()
#    weekday = IntegerField()
#    daysim = IntegerField()
#    secondbin=IntegerField()
#    eventdata=HStoreField()
##    eventdata=JSONField()


config=Configurator("package/config.ini")
fields=[]
#restrictions=["SEG_X","TIME_UTC0_CHARGE", "POWER_AT_PLUG_SLOT","CHARGE_SEGMENT","SLOT_LENGTH_MINUTES","engine_type","modelcode"]
restrictions=["POWER_AT_PLUG_SLOT"]

import sys
# this is a pointer to the module object instance itself.
this = sys.modules[__name__]

# we can explicitly make assignments on it 
this.multicolumn=False



#if config.get_config()["Multicolumn"]:
# datatypes=DataTypeProducer(datatypes_list)
# for datatype in datatypes:
#  for key in datatype.keys():
#   if '_' in key[0]: continue
#   if any(x in key for x in restrictions): continue
#   fields.append(key)
# fields.append("Event")
# this.multicolumn=True

if config.get_config()["Multicolumn"]:
 this.multicolumn=True


datatypes=DataTypeProducer(datatypes_list)
for datatype in datatypes:
 for key in datatype.keys():
  if '_' in key[0]: continue
  if any(x in key for x in restrictions): continue
  fields.append(key)
fields.append("Event")


dyntable = Factory(fields)


# ``create_tables``
# ------------------------
# The ``create_tables`` function is used to create all tables defined in this schema.


def create_tables():
    for cls in globals().values():
        if type(cls) == peewee.BaseModel:
            try:
                cls.create_table()
            #except (peewee.OperationalError,peewee.ProgrammingError,peewee.InternalError) as e:
            except:
                print "exception with ", type(cls),cls
                

#try:
#    print "creating tables!!!!"
#    create_tables()
#except Exception as e :
#    print str(e)
#    pass


# ``recreate_tables``
# ------------------------
# The ``recreate_tables`` function is used to create only the ``Car`` model


def recreate_tables():
 try:
  #if not Cars.table_exists():
  if not dyntable.table_exists():
   #ext_db.create_tables([Cars])
   ext_db.create_tables([dyntable])
 except Exception as e:
  print str(e)
  print(traceback.format_exc())



recreate_tables()
