from package.car import *
from package.events import *
from package.datatypes import *
import package.datatypes as dt
from package.simulator import Simulator
import datetime
import os
import traceback


# Print all queries to stderr.
import logging
logger = logging.getLogger('peewee')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())


DATABASE = {
        'NAME': os.environ.get('DB_NAME'),
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': os.environ.get('DB_PORT'),     
}


print DATABASE

import sys

from  package.configurator import  Configurator
config=Configurator("package/config.ini")

from playhouse.postgres_ext import *
from peewee import *
import peewee
ext_db = PostgresqlExtDatabase(DATABASE['NAME'],host=DATABASE['HOST'],port=DATABASE['PORT'], user=DATABASE['USER'],password=DATABASE['PASSWORD'],register_hstore=False)

class BaseModel(Model):
    class Meta:
        database = ext_db

# the user model specifies its fields (or columns) declaratively, like django
#class Test(BaseModel):
#    username = CharField(unique=True)
#    password = CharField()
#    email = CharField()
#    join_date = DateTimeField()

#    class Meta:
#        order_by = ('username',)

#class Test(BaseModel):
#     username = CharField(unique=True)
#     password = CharField()
#     join_date = DateTimeField()
##     features = HStoreField()
#     features = JSONField()

#def extendclass(dicti):
#    def real_decorator(cls):
#         for name, value in dicti.items():
#          setattr(cls, name, value)
#         return cls
#    return real_decorator

#extention={"password2":CharField()}
#@extendclass(extention)
#class Test(BaseModel):
#     username = CharField(unique=True)
#     password = CharField()
#     join_date = DateTimeField()
##     features = HStoreField()
#     features = JSONField()


#print dir(Test)

#def Factory(fields):
#    class DynamicTable(BaseModel):
#    for field in fields:
#        peewee.DoubleField().add_to_class(DynamicTable, field)
#    return DynamicTable

#def Factory(fields):
#peewee.CharField().add_to_class(Test,"some")
 
def Factory(fields):
    class Test(BaseModel):
        #ref = peewee.ForeignKeyField(StaticTable)
         username = CharField()
         password = CharField()
         join_date = DateTimeField()
         features = JSONField()
    for field in fields:
        peewee.CharField(null=True).add_to_class(Test, field)
    return Test

#fields=["param1","param2"]

from package.datatypes import *
fields=[]
restrictions=["SEG_X","TIME_UTC0_CHARGE", "POWER_AT_PLUG_SLOT","CHARGE_SEGMENT","SLOT_LENGTH_MINUTES","engine_type","modelcode"]
if config.get_config()["Multicolumn"]:
 datatypes=DataTypeProducer(datatypes_list)
 for datatype in datatypes:
  for key in datatype.keys():
   if '_' in key[0]: continue
   if any(x in key for x in restrictions): continue
   fields.append(key)


dyntable = Factory(fields)

#def create_tables():
#    database.connect()
#    database.create_tables([User, Relationship, Message])

#def create_tables():
#  for cls in globals().values():
#    if type(cls) == peewee.BaseModel:
#        try:
#            cls.create_table()
#        except peewee.OperationalError as e:
#            print(e)

#create_tables()
try:
 if not dyntable.table_exists():
   ext_db.create_tables([dyntable])
except Exception as e:
 print str(e)
 print(traceback.format_exc())
 pass

try:
#    with ext_db.transaction():
    with ext_db.atomic():
        # Attempt to create the user. If the username is taken, due to the
        # unique constraint, the database will raise an IntegrityError.
#        user = Test.create(
#            username="test",
#            password="test",
#            email = "test",
#            join_date = datetime.datetime.now()
#        )
        args={"VIN":"testme", "bcBeTimeEndUtc":"111111","aasas":"asasa"}
        print datetime.datetime.now()
	user = dyntable.create(
		username="test3", 
		password="test2",
                join_date = datetime.datetime.now(),
		features = {'garage': '2 cars', 'bath': '2 bath'},
		**args
        )

except Exception as e:
    print "HAA"
    print str(e)
    print(traceback.format_exc())
    print "HAA"

sys.exit(1)

config = {
    "ThreadPoolSize":3000, # fix an architecture of the car
    "metropolyLocation":(48.106178, 11.73692),
    "metropolyBBOX":{"NE":(48.299469, 11.85201),"SW":(47.912891, 11.38549)},
    "metropolyPrecision":5,
#    "numCars":1000,
    "numCars":10,
    "numSimulations":2,
#    "RealTimeSimulation":False,
    "RealTimeSimulation":True,
    "virtualTimeFactor":0.001
}

#sims=Simulator(config)
#
#sims.runRealTime()
#sims.run()


from package.configurator import *
from package.model import recreate_tables


recreate_tables()
config=Configurator("package/config.ini")
config=config.get_config()
sims=Simulator(config)
sims.run()

