# test of the db

from playhouse.postgres_ext import *
from playhouse.pool import PooledPostgresqlExtDatabase
from peewee import *
import peewee
import datetime
import os
import traceback
import sys
import time
from blinker import Signal




##########

IS_PY2 = sys.version_info < (3, 0)

if IS_PY2:
    from Queue import Queue
else:
    from queue import Queue

from threading import Thread, current_thread
import time
import  pqueue as pq
import cPickle
import zlib
import traceback
import sys
import threading

    
#################Thread Pool ########################
class Worker(Thread):
    """ Thread executing tasks from a given tasks queue """
    def __init__(self, tasks):
        Thread.__init__(self)
        self.tasks = tasks
        self.daemon = True
        self.start()

    def run(self):
        while True:
            func, args, kargs = self.tasks.get()
            try:
                func(*args, **kargs)  
            except Exception as e:
                # An exception happened in this thread
                print "Exception here-->",str(e)
                print(traceback.format_exc())
            finally:
                # Mark this task as done, whether an exception happened or not
                self.tasks.task_done()


class ThreadPool:
    """ Pool of threads consuming tasks from a queue """
    def __init__(self, num_threads):
        self.tasks = Queue(num_threads)
        
        self.num_threads = num_threads
        for _ in range(num_threads):
            Worker(self.tasks)
            
    def add_task(self, func, *args, **kargs):
        """ Add a task to the queue """
        self.tasks.put((func, args, kargs))

    def map(self, func, args_list):
        """ Add a list of tasks to the queue """
        for args in args_list:
            self.add_task(func, args)

    def wait_completion(self):
        """ Wait for completion of all the tasks in the queue """
        self.tasks.join()
        
        

######### Database #######################

DATABASE = {
        'NAME': os.environ.get('DB_NAME'),
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': os.environ.get('DB_PORT'),
}

ext_db = PooledPostgresqlExtDatabase(database=DATABASE['NAME'],host=DATABASE['HOST'],port=DATABASE['PORT'], user=DATABASE['USER'],password=DATABASE['PASSWORD'],threadlocals=True,register_hstore=True,max_connections=900,stale_timeout=10)

success=Signal()
failed=Signal()

class BaseModel(Model):
    class Meta:
        database = ext_db

# the user model specifies its fields (or columns) declaratively, like django
class Test(BaseModel):
    username = CharField()
    password = CharField()
    class Meta:
        order_by = ('username',)



try:
 if not Test.table_exists():
   ext_db.create_tables([Test])
except Exception as e:
 print str(e)
 print(traceback.format_exc())
 pass

######################################## Utils

class RepeatedTimer:
    
    '''
    the class responsible for repeating some function in time (per 1 s second), for example
    >timer = RepeatedTimer(1, lambda: print_percent(total, BIGFILE_SIZE))
    >timer.start()
    >try:
    >    while 1:
    >        f.write(chunk)
    >        total += chunk_len
    >        if total >= BIGFILE_SIZE:
    >            break
    >finally:
    >    f.close()
    >    timer.stop()
    '''

    def __init__(self, timeout, fun):
        self.timeout = timeout
        self.fun = fun

    def start(self):
        
        def main():
            self.fun()
            self.start()

        self.timer = threading.Timer(1, main)
        self.timer.start()


    def stop(self):
        self.timer.cancel()



################################





import redis
import redis_lock
from logger import mylogging
from configurator import Configurator
config=Configurator('package/config.ini').get_config()
config_fields=["ThreadPoolSize","numCars","RealTimeSimulation"]
mylogging.setSimConfig(dict(zip(config_fields, [config[k] for k in config_fields])))
_mylogging=lambda x: mylogging.setVerbosity('INFO')(x)

class DatabaseManager(object):

  successed = Signal()
  failed= Signal()
  redis_host = "redis"
  redis_port = 6379
  myr = redis.StrictRedis(host=redis_host, port=redis_port, db=0)
  ok=0
  bad=0
  ok_id=[]
  bad_id=[]
  prev_ok=0
  prev_bad=0
  timer = RepeatedTimer(1, lambda: DatabaseManager.timerjob())
  timer.start()

#  def __init__(self,r):
#    pass
#    self.r=r
#    self.ok=0
#    self.bad=0
  @staticmethod
  def  timerjob():
    _mylogging("Successul commits %d  per 1s "%(len(DatabaseManager.ok_id)-DatabaseManager.prev_ok))
    DatabaseManager.prev_ok=len(DatabaseManager.ok_id) 
    _mylogging("Failed commits %d  per 1s "%(len(DatabaseManager.bad_id)-DatabaseManager.prev_bad))
    DatabaseManager.prev_bad=len(DatabaseManager.bad_id) 
    _mylogging("Total Successul commits %d"%DatabaseManager.prev_ok)
    _mylogging("Total Failed commits %d"%DatabaseManager.prev_bad)


  @staticmethod
  @successed.connect
  def success(sender_id):
   #print ("worker %d OK!" %(sender_id))
   #with redis_lock.Lock(DatabaseManager.myr, "_ok_lock_"):
   DatabaseManager.ok+=1
   DatabaseManager.ok_id.append(sender_id)

  @staticmethod
  @failed.connect
  def fail(sender_id):
#   print ("worker %d NOT_OK!" %(sender_id))
   #with redis_lock.Lock(DatabaseManager.myr, "_notok_lock_"):
   DatabaseManager.bad+=1
   DatabaseManager.bad_id.append(sender_id)

  def __repr__(self):
   return str({"OK commits":DatabaseManager.ok,"BAD":DatabaseManager.bad, 
		#"ok_id":DatabaseManager.ok_id,
		#"bad_id":DatabaseManager.bad_id,
	})
  
  def  __del__(self):
    _mylogging( "stoping timer" )
    DatabaseManager.timer.stop()


#def test_db(ext_db,id_num,success,failed,manager):
def test_db(ext_db,id_num,success,failed):

 try:
   with ext_db.execution_context() as ctx:
#    with ext_db.transaction():
#    with ext_db.atomic():
        # Attempt to create the user. If the username is taken, due to the
        # unique constraint, the database will raise an IntegrityError.
        user = Test.create(
            username="test",
            password="test",
#            email = "test",
#            join_date = datetime.datetime.now()
        )
#        success.send(id_num)
#        time.sleep(1)
        DatabaseManager.successed.send(id_num)
#        DatabaseManager.success.send(id_num)
 except Exception as e:
#    print "HAA"
    #print str(e)
#    print(traceback.format_exc())
#    print "HAA"
#    failed.send(id_num)         
#     DatabaseManager.fail.send(id_num)
    DatabaseManager.failed.send(id_num)

 #finally:
    #ext_db._close(ext_db.get_conn())
    #print "worker %d done"%(id_num)





@success.connect
def ok(sender_id):
 print ("worker %d OK!" %(sender_id))

@failed.connect
def not_ok(sender_id):
 print ("worker %d NOT_OK!" %(sender_id))


#test_db(ext_db)

pool = ThreadPool(130)
#def test_worker(x):
# time.sleep(1)
# print  "worker: "+str(x)

#for i in range(10):
# print i
# pool.add_task(test_worker,i)

#redis_host = "redis"
#redis_port = 6379
#r = redis.StrictRedis(host=redis_host, port=redis_port, db=0)
#manager=DatabaseManager(r)
manager=DatabaseManager()

for i in range(1450):
 #print "Submit request %d"%i
 #pool.add_task(test_db,ext_db,i,success,failed,manager)
 pool.add_task(test_db,ext_db,i,success,failed)

pool.wait_completion()
print repr(manager)
del  manager
#class Test2(object):
# test = Signal()

# @test.connect
# def get(id):
#    print "got ",id


#def sendme(obj):
#  obj.test.send(1)
 
#obj=Test2()
#sendme(obj)

sys.exit(1)

