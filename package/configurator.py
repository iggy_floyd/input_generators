#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Configurator
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   configurator.py
#

'''
	Configurator

'''

__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::


try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0
    
    
# ``AttrDict``
# ----------------------------------------
# The ``AttrDict`` class is a help type which represents internal storage of the ``ConfigParser`` class.
#

class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


# ``Configurator`` class
# ----------------------------------------
# The ``Configurator`` class reads configuration from the file
#

class Configurator(object):
    def __init__(self,filename):
        # instantiate
        self.configini = ConfigParser(dict_type=AttrDict)
        self.configini.read(filename)
 
    def get_config(self):
        return {
              "metropolyLocation":(48.106178, 11.73692),
              "metropolyBBOX":{"NE":(48.299469, 11.85201),"SW":(47.912891, 11.38549)},
		# limited by postgresql
              "ThreadPoolSize":self.configini.getint('Simulation', 'SizePoolThreads'),
              #"ThreadPoolSize":30,
              "metropolyPrecision":self.configini.getint('Metropolis','Precision'),
              "numCars":self.configini.getint('Metropolis','Cars'),
              "numSimulations":self.configini.getint('Simulation','Duration'),
              "RealTimeSimulation":self.configini.getboolean('Simulation','RealTimeSimulation'),
              "virtualTimeFactor":self.configini.getfloat('Simulation','VirtualTimeFactor'),
	      "Multicolumn":self.configini.getboolean('Model','Multicolumn'),
              "MetropolisSize":self.configini.get('Metropolis','Size')
            }
