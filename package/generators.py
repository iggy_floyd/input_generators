#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Event Data Generators
# ==================================================================
#
# :Date: Dec 29, 2016, 12:59:09 PM
# :File:   generators.py
#

'''
	Generators definition

'''

__author__="Igor Marfin <Robotron Gmbh, 2016> igor.marfin@robotron.de"
__date__ ="$Dec 29, 2016, 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::


from scipy.stats import poisson
import numpy as np
from .utils import *
import datetime 
import time
import random


    
# ``VINCodeGenerator``
# ----------------------------------------
# The ``VINCodeGenerator`` generator is responsible for VIN code producing.

def VINCodeGenerator():
    '''
    Generates VIN codes
    '''
    import string
    import random
    
    """ Erste 3 Stellen =>X """    
    X='XXX'
    ModelCode = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(4))
    RandomCode = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(7))
    VinCode=X+ModelCode+X+RandomCode
    return (VinCode,len(VinCode))

    
# ``maxBatterySizeGenerator``
# ----------------------------------------
# The ``maxBatterySizeGenerator`` generator produces maxBatterySize value.


def maxBatterySizeGenerator():
    '''pseudo random generation of the maxBaterySize'''
    max_val_uint16 = 65535 # http://www.cplusplus.com/reference/climits/

    # pseudo data according specification: Rand[50% - 0; 25%-8000; 25%-30000]
    data =  np.append(np.array([0]*500),np.array([8000]*250))
    data =  np.append(data,np.array([30000]*250))

    # bins 
    bins = bins=[0,8000,30000,max_val_uint16]
    # helper function: return either 0, 8000 or 30000
    get_category=lambda x:  0 if x<8000. else (8000 if x<30000 else 30000)
    return (get_category(generate_from_histogram(data,1,bins)[0]),2)


# ``XXXGenerator``
# --------------------
# All generators, determined bellow, randomly produce different properties of event data. 

EngineTypeGenerator = lambda : RandomIntNumberSeqGenerator(minval=1,maxval=6,lendigits=4)
ModelCodeGenerator = lambda : RandomIntNumberSeqGenerator(minval=1,maxval=50,lendigits=8)

RandomAvrageSpeedGenerator = lambda : RandomFloatNumberGenerator(minval=5.,maxval=100.,stepsize=0.01)
RandomBeHvSocStartGenerator = lambda : RandomFloatNumberGenerator(minval=0.,maxval=100.,stepsize=0.5)
RandomBeHvSocEndGenerator = lambda : RandomFloatNumberGenerator(minval=0.,maxval=100.,stepsize=0.5)
RandomBeTankLevelStartGenerator = lambda : RandomFloatNumberGenerator(minval=20.,maxval=80.,stepsize=0.5)
RandomBeTankLevelEndGenerator = lambda x: (float(x)-RandomFloatNumberGenerator(minval=0.,maxval=float(x),stepsize=0.5)[0],4)

#SEG_X_SOC_CUST_PLUG, Rand(0-100;0.5)
RandomSEGXSOCCUSTPLUGGenerator = lambda : RandomFloatNumberGenerator(minval=0.,maxval=100.,stepsize=0.5)
#SEG_X_SOC_CUST_UNPLUG, Rand(0-100;0.5)
RandomSEGXSOCCUSTUNPLUGGenerator = lambda : RandomFloatNumberGenerator(minval=0.,maxval=100.,stepsize=0.5)

#bcBeTimeStartLocal, any time/day between now and 10 days ahead
RandomBeTimeStartLocalGenerator =  lambda : RandomDatetimeInSecs(start=datetime.datetime.now(),deltadatetime = datetime.timedelta(days=10))
# bcBeTimeEndLocal, any time/day between bcBeTimeStartLocal and 1 day ahead
RandomBeTimeEndLocalGenerator =  lambda x: RandomDatetimeInSecs(start=datetime.datetime.fromtimestamp(x),deltadatetime = datetime.timedelta(days=1))

#bcBeTimeStartUtc, UTC time from bcBeTimeStartLocal
RandomBeTimeStartUTCGenerator = lambda x: (int(time.mktime(local_to_utc(datetime.datetime.fromtimestamp(x)).timetuple())),8)


#bcBeTripDistanceKm
RandomBeTripDistanceKmGenerator = lambda: (RandomTrunctatedNormalGenerator(40.,15.,1.,100.),4)
#bcBeRecuperationAll
RandomBeRecuperationAllGenerator = lambda: (RandomTrunctatedNormalGenerator(40.,15.,0.,20.),4)
#bcBeNvConsAll
RandomBeNvConsAllGenerator = lambda: (RandomTrunctatedNormalGenerator(40.,15.,0.,5.),4)

# Rand(0-80;0.05)/100)*Trip_distanz
RandomBeTripElectricKmGenerator = lambda x: (RandomFloatNumberGenerator(minval=0.,maxval=80.,stepsize=0.05)[0]/100.*x,4)

#bcBeConsFuel_visible
RandomBeConsFuelVisibleGenerator = lambda: (FVerteilung(5.,20.),4)
#bcBeConsElectric_visible
RandomBeConsElectricVisibleGenerator = lambda: (FVerteilung(12.,30.),4)
#bcBeConsElectricAll
RandomBeConsElectricAllGenerator = lambda: (FVerteilung(12.,30.),4)
#bcBeSegmentId
#RandomBeSegmentIdGenerator= lambda: (np.random.random_integers(0,1000,1)[0],1)
# with memory
RandomBeSegmentIdGenerator= lambda: (setattr(RandomBeSegmentIdGenerator,"val",np.random.random_integers(0,1000,1)[0]),getattr(RandomBeSegmentIdGenerator,"val"),1)[1:3]

#test1= lambda: (setattr(test1,"val",np.random.random_integers(0,1000,1)[0]),getattr(test1,"val"),1)[1:3]
#test2= lambda: (np.random.random_integers(0,1000,1)[0],1) if not hasattr(test1,"val")  else (test1.val,1)

#bcBeDriverId
RandomBeDriverIdGenerator= lambda: (np.random.random_integers(0,100000,1)[0],16)


RandomGPSStartLatGenerator = lambda: (_ for _ in ()).throw(NotImplementedError)
RandomGPSStartLonGenerator = lambda: (_ for _ in ()).throw(NotImplementedError)
RandomGPSEndLatGenerator = lambda: (_ for _ in ()).throw(NotImplementedError)
RandomGPSEndLonGenerator = lambda: (_ for _ in ()).throw(NotImplementedError)


#SEG_X_PLUG_SEGMENT_ID: rand(min-max)
RandomSEGXPLUGSEGIDGenerator= lambda: (np.random.random_integers(0,100000,1)[0],1)
# SEG_X_TIME_UTC0_PLUG: Simulationszeit?
RandomSEGXTIMELocalPlugGenerator = lambda : DatetimeInSecs()


RandomLatSEGXPOSGenerator = lambda: (_ for _ in ()).throw(NotImplementedError)
RandomLonSEGXPOSGenerator = lambda: (_ for _ in ()).throw(NotImplementedError)

#SEG_X_ENERGY_CONS_FROM_POWERGRID
#RandomSEGXENERGYCONSFROMPOWERGRIDGenerator = lambda: (_ for _ in ()).throw(NotImplementedError)

#SEG_X_CHARGE_SEGMENT_ID Rand(0-255)
RandomSEGXCHARGESEGIDGenerator =  lambda: (np.random.random_integers(0,255,1)[0],1)
#SEG_X_TIME_UTC0_START
RandomSEGXTIMELocalStartGenerator = lambda : DatetimeInSecs()

#SEG_X_SOC_CUST_START, Rand(0-100;0.5)
RandomSEGXSOCCUSTSTARTGenerator = lambda : RandomFloatNumberGenerator(minval=0.,maxval=100.,stepsize=0.5)
#SEG_X_SOC_CUST_END: Start+rand(Start-100;0.5)
RandomSEGXSOCCUSTENDGenerator = lambda x: (float(x)+RandomFloatNumberGenerator(minval=float(x),maxval=100.,stepsize=0.5)[0],4)

#SEG_X_ENERGY_CONS_FROM_POWERGRID: rand(0-6552;0.1)
RandomSEGXENERGYCONSFROMPOWERGRIDGenerator = lambda : RandomFloatNumberGenerator(minval=0.,maxval=6552.,stepsize=0.1)

#SLOT_LENGTH_MINUTES
RandomSLOTLENGTHMINUTESGenerator =  lambda: (_ for _ in ()).throw(NotImplementedError)

# POWER_AT_PLUG_SLOT rand(0-150;0.1)
RandomPOWERATPLUGSLOTGenerator = lambda : RandomFloatNumberGenerator(minval=0.,maxval=150.,stepsize=0.1)

#bcBeSegmentIdTankhistorie correlated with RandomBeSegmentIdGenerator!
RandomBeSegmentIdTankhistorieGenerator= lambda: (np.random.random_integers(0,1000,1)[0],1) if not hasattr(RandomBeSegmentIdGenerator,"val")  else (RandomBeSegmentIdGenerator.val,1)


