from package.car import *
from package.events import *
from package.datatypes import *
import package.datatypes as dt
from package.simulator import Simulator

import os


DATABASE = {
        'NAME': os.environ.get('DB_NAME'),
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': os.environ.get('DB_PORT'),
}



from playhouse.postgres_ext import *
from peewee import *
import peewee
ext_db = PostgresqlExtDatabase(DATABASE['NAME'],host=DATABASE['HOST'],port=DATABASE['PORT'], user=DATABASE['USER'],register_hstore=False)

class BaseModel(Model):
    class Meta:
        database = ext_db

# the user model specifies its fields (or columns) declaratively, like django
class Test(BaseModel):
    username = CharField(unique=True)
    password = CharField()
    email = CharField()
    join_date = DateTimeField()

    class Meta:
        order_by = ('username',)

#def create_tables():
#    database.connect()
#    database.create_tables([User, Relationship, Message])

#def create_tables():
#  for cls in globals().values():
#    if type(cls) == peewee.BaseModel:
#        try:
#            cls.create_table()
#        except peewee.OperationalError as e:
#            print(e)

#create_tables()

try:
 ext_db.create_tables([Test])
except:
 pass


config = {
    "ThreadPoolSize":3000, # fix an architecture of the car
    "metropolyLocation":(48.106178, 11.73692),
    "metropolyBBOX":{"NE":(48.299469, 11.85201),"SW":(47.912891, 11.38549)},
    "metropolyPrecision":5,
    "numCars":1000,
#    "numCars":10,
    "numSimulations":2,
#    "RealTimeSimulation":False,
    "RealTimeSimulation":True,
    "virtualTimeFactor":0.001
}

sims=Simulator(config)
#
#sims.runRealTime()
sims.run()
