#!/usr/bin/env bash

echo "doing db for" ${DB_USER} ${DB_NAME};
createuser -U postgres --createdb --createrole ${DB_USER};
createdb -U ${DB_USER} ${DB_NAME};
echo "CREATE EXTENSION hstore;" |  psql -U postgres ${DB_NAME};
