import os
import time
from celery import Celery
import celery.states as states

### a new approach of simulation and prediction
from celery.signals import task_revoked
import redis
numSim=-1

env=os.environ
CELERY_BROKER_URL=env.get('CELERY_BROKER_URL','redis://localhost:6379'),
CELERY_RESULT_BACKEND=env.get('CELERY_RESULT_BACKEND','redis://localhost:6379')


celery= Celery('tasks',
                broker=CELERY_BROKER_URL,
                backend=CELERY_RESULT_BACKEND)

redis_host = "redis"
redis_port = 6379
r= redis.StrictRedis(host=redis_host, port=redis_port, db=0)
r.set('numSim',numSim)

@task_revoked.connect
def revoked_handler(*args, **kwargs):
    # get the task that was revoked
    from celery.utils.log import get_task_logger
    import redis
    from lockfile import LockFile
    #global numSim

    redis_host = "redis"
    redis_port = 6379
    r= redis.StrictRedis(host=redis_host, port=redis_port, db=0)
    numSim=int(r.get('numSim'))
    logger = get_task_logger(__name__)
    logger.info("!!!!!!!!!!!!!")
    logger.info(kwargs)
    taskObj = kwargs.get('request').task
    taskObjName = kwargs.get('request').task_name
    taskObjId = kwargs.get('request').task_id
    logger.info(taskObj)
    logger.info("aaasasa "+taskObjName)
    logger.info("aaasasa "+taskObjId)
    if "simulate" in str(taskObjName):
     uuid = r.get("_predict_id_")
     celery.control.revoke(uuid, terminate=True)
     r.delete("_predict_id_")
     r.delete("_simulate_id_")
     r.set('terminate',1)  
     mylogging.setVerbosity('ERROR')("SIMULATION %d: SIMULATION_INTERRUPTED"%numSim)
    if "predict" in str(taskObjName):
     uuid = r.get("_simulate_id_")
     celery.control.revoke(uuid, terminate=True)
     r.delete("_predict_id_")
     r.delete("_simulate_id_")
     r.set('terminate',1)  
     mylogging.setVerbosity('ERROR')("SIMULATION %d: PREDICTION_INTERRUPTED"%numSim)
    lock = LockFile("/tmp/lock")
    try:
     lock.break_lock()
    except:
     pass




##### simulation task
from package.car import *
from package.events import *
from package.datatypes import *
import package.datatypes as dt
from package.simulator import Simulator

from package.configurator import *
from package.model import recreate_tables,multicolumn
from package.logger import mylogging

import os


DATABASE = {
        'NAME': os.environ.get('DB_NAME'),
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': os.environ.get('DB_PORT'),
}



from playhouse.postgres_ext import *
from peewee import *
import peewee

#ext_db = PostgresqlExtDatabase(DATABASE['NAME'],host=DATABASE['HOST'],port=DATABASE['PORT'], user=DATABASE['USER'],password=DATABASE['PASSWORD'],register_hstore=True)
#class BaseModel(Model):
#    class Meta:
#        database = ext_db

# a new simulation stuff: v3
from functools import partial

def run(self,r,channel,taskinst,numSim):
        import redis
        from package.RedisListener import Listener
        from celery.utils.log import get_task_logger
#        global numSim
        logger = get_task_logger(__name__)
        multicolumn=self.config["Multicolumn"]
        if self.config["RealTimeSimulation"]:
            logger.info("realtime")
            this=self
            #define the simulation procedure
            def simulation(self,item,sims,numSim):
               global logger
               import pickle
               import random
               import time
               logger.info("we are processing %s"%item["data"])
               logger.info(str(item))
               if item["type"] != "message":
                 return
               logger.info(str(item))
               try: 
                 data=pickle.loads(self.redis.get(item["data"]))
               except:
                 return
               logger.info("Start simulation of the day")
               sims.simulateDay_v2(data,numSim)
               logger.info( "removing dataset....")
               logger.info(self.redis.delete(item["data"]))  
               logger.info( "job is done")

            #mysimulation = partial(simulation, sims=self)
            mysimulation = lambda self,item: simulation(self,item,this,numSim)
            #replace a 'standard' procedure by 'simulation' one in the RedisListener
            Listener.work = mysimulation
            client = Listener(r, [channel])
            client.start()
            task=celery.send_task('mytasks.predict', args=[channel])
            logger.info( "predict id stored")
            r.set("_predict_id_",task.id)
            logger.info( "simulate id stored")
            r.set("_simulate_id_",taskinst.request.id)
            client.wait()
            r.delete("_predict_id_")
            logger.info( "predict id removed")
            r.delete("_simulate_id_")
            logger.info( "simulate id removed")                        
        else:
            logger.info("LoadBalance simulation")
            r.set("_simulate_id_",taskinst.request.id)
            self.runLoadBalance(numSim)
            r.delete("_simulate_id_")
            logger.info( "simulate id removed")

@celery.task(name='mytasks.simulate',bind=True)
def simulate(self):
    #global numSim
    import redis
    from package.RedisListener import Listener
    from celery.utils.log import get_task_logger
    logger = get_task_logger(__name__)
    from lockfile import LockFile
    redis_host = "redis"
    redis_port = 6379
    channel="simulate"
    r= redis.StrictRedis(host=redis_host, port=redis_port, db=0)
    numSim=int(r.get('numSim'))
    numSim+=1

    lock = LockFile("/tmp/lock")
    if lock.is_locked():
     mylogging.setVerbosity('ERROR')("SIMULATION_IN_PROGRESS")
     raise Exception("Simulation in progress...")
    else:
     mylogging.setVerbosity('INFO')("SIMULATION %d: SIMULATION_STARTED"%numSim)
     lock.acquire()
    r.set('terminate',0)
    r.set('numSim',numSim)
    recreate_tables()
    config=Configurator("package/config.ini")
    config=config.get_config()
    Simulator.run=run
    sims=Simulator(config)
    sims.run(r,channel,self,numSim)
    try:
     lock.release()
    except:
     pass
    mylogging.setVerbosity('INFO')("SIMULATION %d: SIMULATION_ENDED"%numSim)
    return "Done"



@celery.task(name='mytasks.predict')
def predict(channel):
    import redis
    from package.utils import makeUUID
    from banyan import SortedSet
    import random
    import time
    import pickle
    from celery.utils.log import get_task_logger
    logger = get_task_logger(__name__)
    redis_host = "redis"
    redis_port = 6379
    r= redis.StrictRedis(host=redis_host, port=redis_port, db=0)
    config=Configurator("package/config.ini")
    config=config.get_config()
    sims=Simulator(config)
  
    logger.info("starting prediction")
    for i in range(sims.config["numSimulations"]):
      sortedset = SortedSet([])
      logger.info("prediction %d"%i)  
      sims.predictDay_v2(sortedset)
      logger.info("pickling")  
      pickled_object = pickle.dumps(sortedset)
      key=makeUUID() 
      logger.info("set key %s"%key)
      r.set(key,pickled_object)
      logger.info("publish key %s"%key)
      r.publish(channel,key)          
    # close the channel
    r.publish(channel,"KILL")
    return "Done"


@celery.task(name='mytasks.terminate')
def terminate():
  import redis
  from lockfile import LockFile
  redis_host = "redis"
  redis_port = 6379
  r= redis.StrictRedis(host=redis_host, port=redis_port, db=0)
  uuid = r.get("_predict_id_")
  celery.control.revoke(uuid, terminate=True)
  uuid = r.get("_simulate_id_")
  celery.control.revoke(uuid, terminate=True)
  r.delete("_simulate_id_")
  r.delete("_predict_id_")
  lock = LockFile("/tmp/lock")
  r.set('terminate',1)  
  try:
   lock.break_lock()
  except:
   pass
  return "Done"

